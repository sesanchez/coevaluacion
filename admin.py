from django.contrib.admin.sites import AdminSite
from django.views.decorators.cache import never_cache
from django.conf import settings
from django.utils.translation import ugettext as _
from django.contrib.admin.forms import AdminAuthenticationForm


class CoevaluacionAdminSite(AdminSite):
    @never_cache
    def login(self, request, extra_context=None):
        """
        Displays the login form for the given HttpRequest.
        """
        from django.contrib.auth.views import login
        context = {
            'title': _('Log in'),
            'app_path': request.get_full_path(),
            'redirect_field_name': settings.ADMIN_LOGIN_REDIRECT_URL,
        }
        context.update(extra_context or {})

        defaults = {
            'extra_context': context,
            'current_app': self.name,
            'authentication_form': self.login_form or AdminAuthenticationForm,
            'template_name': self.login_template or 'registration/login.html',
        }
        return login(request, **defaults)
    
    @never_cache
    def logout(self, request, extra_context=None):
        """
        Logs out the user for the given HttpRequest.

        This should *not* assume the user is already logged in.
        """
        from django.contrib.auth.views import logout
        context = {
            'next_page': settings.ADMIN_LOGOUT_URL,
        }
        context.update(extra_context or {})
        defaults = {
            'current_app': self.name,
            'extra_context': extra_context or {},
        }
        if self.logout_template is not None:
            defaults['template_name'] = self.logout_template
        return logout(request, **defaults)

site = CoevaluacionAdminSite()