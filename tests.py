from django.test.client import Client
from django.test.testcases import TestCase
from django.utils.translation import ugettext as _
from django.core.urlresolvers import reverse
from django.contrib.auth.forms import AuthenticationForm
from cursos.models import Profesor, Estudiante
from django.contrib.auth.models import Group, User
from django.core import mail



class NoLoginTest(TestCase):

	def setUp(self):
		pass

	def tearDown(self):
		pass

	def test_no_login(self):
		url = reverse('login_view')
		c = Client(enforce_csrf_checks=True)
		c.get(url, follow=True)
		csrf_token = c.cookies['csrftoken'].value
		response = c.post(url, {'csrfmiddlewaretoken': csrf_token,'username': 'username', 'password': 'password'})
		self.assertEqual(response.status_code, 200)
		self.assertIsInstance(response.context['form'], AuthenticationForm)
		self.assertEqual(len(response.context['form'].errors), 1)


class LoginProfesorTest(TestCase):
	fixtures = ['groups']

	def setUp(self):
		self.user = Profesor.objects.create_user(username='testprofesor', password='testprofesor')
		self.user.is_active = True
		self.user.is_staff = True
		group = Group.objects.get(name='Profesor') 
		self.user.groups.add(group)
		self.user.save()
		self.url = reverse('login_view')
		#expected_url = reverse('admin_coevaluaciones:ver_coevaluaciones_profesor')	
		self.c = Client(enforce_csrf_checks=True)
		self.c.get(self.url, follow=True)
		self.csrf_token = self.c.cookies['csrftoken'].value		

	def tearDown(self):
		self.user.delete()

	def test_profesor_login_sucess(self):
		expected_url = reverse('homeredirect')
		response = self.c.post(self.url, {'csrfmiddlewaretoken': self.csrf_token,'username': 'testprofesor', 'password': 'testprofesor'})
		self.assertRedirects(response, expected_url,302,302)
		
	def test_profesor_login_profesor_view(self):
		response = self.c.post(self.url, {'csrfmiddlewaretoken': self.csrf_token,'username': 'testprofesor', 'password': 'testprofesor'}, follow=True)
		self.assertEqual(response.status_code, 200)
		self.assertIn('usuario',response.context)
		self.assertIn('rol', response.context)
		self.assertEqual(response.context['rol'],'profesor')

class LoginAlumnoTest(TestCase):
	fixtures = ['groups']

	def setUp(self):
		self.user = Estudiante.objects.create_user(username='testalumno', password='testalumno')
		self.user.is_active = True
		self.user.is_staff = True
		group = Group.objects.get(name='Alumno') 
		self.user.groups.add(group)
		self.user.save()
		self.url = reverse('login_view')
		#expected_url = reverse('admin_coevaluaciones:ver_coevaluaciones_profesor')	
		self.c = Client(enforce_csrf_checks=True)
		self.c.get(self.url, follow=True)
		self.csrf_token = self.c.cookies['csrftoken'].value		

	def tearDown(self):
		self.user.delete()

	def test_alumno_login_sucess(self):
		expected_url = reverse('homeredirect')
		response = self.c.post(self.url, {'csrfmiddlewaretoken': self.csrf_token,'username': 'testalumno', 'password': 'testalumno'})
		self.assertRedirects(response, expected_url,302,302)
		
	def test_alumno_login_alumno_view(self):
		response = self.c.post(self.url, {'csrfmiddlewaretoken': self.csrf_token,'username': 'testalumno', 'password': 'testalumno'}, follow=True)
		self.assertEqual(response.status_code, 200)
		self.assertIn('usuario',response.context)
		self.assertIn('rol', response.context)
		self.assertEqual(response.context['rol'],'estudiante')


class LoginAdminTest(TestCase):
	fixtures = ['groups']

	def setUp(self):
		self.user = Estudiante.objects.create_user(username='testadmin', password='testadmin')
		self.user.is_active = True
		self.user.is_staff = True
		self.user.is_admin = True
		self.user.save()
		self.url = reverse('login_view')
		#expected_url = reverse('admin_coevaluaciones:ver_coevaluaciones_profesor')	
		self.c = Client(enforce_csrf_checks=True)
		self.c.get(self.url, follow=True)
		self.csrf_token = self.c.cookies['csrftoken'].value		

	def tearDown(self):
		self.user.delete()

	def test_admin_login_sucess(self):
		expected_url = reverse('homeredirect')
		response = self.c.post(self.url, {'csrfmiddlewaretoken': self.csrf_token,'username': 'testadmin', 'password': 'testadmin'})
		self.assertRedirects(response, expected_url,302,302)
		
	def test_admin_login_admin_view(self):
		response = self.c.post(self.url, {'csrfmiddlewaretoken': self.csrf_token,'username': 'testadmin', 'password': 'testadmin'}, follow=True)
		self.assertEqual(response.status_code, 200)
		self.assertIn('user', response.context)
		self.assertEqual(response.context['user'].username,'testadmin')
		
class TestPaswordMailTest(TestCase):
	fixtures = ['groups']

	def setUp(self):
		self.user = User.objects.create_user(username='testmail', password='testmail',email='test@example.com')
		self.user.is_active = True
		self.user.is_staff = True
		self.user.save()
		self.url = reverse('password_reset')
		#expected_url = reverse('admin_coevaluaciones:ver_coevaluaciones_profesor')	
		self.c = Client(enforce_csrf_checks=True)
		self.c.get(self.url, follow=True)
		self.csrf_token = self.c.cookies['csrftoken'].value		

	def tearDown(self):
		self.user.delete()

	def test_mail_password_recover_sucess(self):
		expected_url = reverse('password_reset_done')
		response = self.c.post(self.url, {'csrfmiddlewaretoken': self.csrf_token,'email': 'test@example.com'})
		self.assertRedirects(response, expected_url,302)
		self.assertEqual( len(mail.outbox), 1 )  # @UndefinedVariable
		self.assertIn('testmail', mail.outbox[0].body)
		


	
        