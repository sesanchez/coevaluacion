from django.conf.urls import patterns, include, url
from django.contrib import admin
from django.contrib.auth.decorators import login_required
from django.contrib.auth.views import login, logout, password_change
from admin import site
from views import login_view, homeredirect
from cursos.views import PrimerAccesoEstudiante

admin.autodiscover()
admin.site.login = site.login
admin.site.logout = site.logout

urlpatterns = patterns('',
	# Examples:
	# url(r'^$', 'coevaluaciones.views.home', name='home'),
	# url(r'^coevaluaciones/', include('coevaluaciones.foo.urls')),

	# Uncomment the admin/doc line below to enable admin documentation:
	# url(r'^admin/doc/', include('django.contrib.admindocs.urls')),

	# Uncomment the next line to enable the admin:
	
	#url(r'^$', login_view),
	
	url(r'^success/$', homeredirect, name='homeredirect'),
	url(r'^$', login_view, name='login_view'),
	url(r'^logout/$', 'django.contrib.auth.views.logout',{'next_page': '/'},name='logout'),
    url(r'^usuario/password/olvidar/$', 'django.contrib.auth.views.password_reset', 
    	{'post_reset_redirect' : '/usuario/password/olvidar/exito/'}, 
    	name="password_reset"),
    url(r'^usuario/password/olvidar/exito/$','django.contrib.auth.views.password_reset_done',name="password_reset_done"),
    url(r'^usuario/password/olvidar/(?P<uidb36>[0-9A-Za-z]+)-(?P<token>.+)/$',
    	'django.contrib.auth.views.password_reset_confirm', 
    	{'post_reset_redirect' : '/usuario/password/exito/'}),
	url(r'^usuario/password/exito/$','django.contrib.auth.views.password_reset_complete'),
    
	url(r'^usuario/email/cambiar/$', 'django.contrib.auth.views.password_change', name='change_email'),
	url(r'^alumno-primer-acceso/(?P<pk>\d+)/(?P<hash_password>\S+)/$',PrimerAccesoEstudiante.as_view() ,name='first_access'),
	
	(r'^admin/admin/', include(admin.site.urls)),
	(r'^', include('coevaluaciones.admin.urls', namespace='admin_coevaluaciones')),
	(r'^', include('cursos.admin.urls', namespace='admin_cursos')),


)
