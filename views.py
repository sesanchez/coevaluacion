from django.core.urlresolvers import reverse
from django.contrib.auth.decorators import login_required
from django.views.decorators.cache import never_cache
from django.views.decorators.csrf import csrf_protect
from django.contrib.auth import REDIRECT_FIELD_NAME
from django.contrib.auth.forms import AuthenticationForm
from django.shortcuts import redirect
from django.contrib.auth.views import login
from django.http.response import HttpResponseRedirect


@csrf_protect
@never_cache
def login_view(request, template_name='registration/login.html',
		  redirect_field_name=REDIRECT_FIELD_NAME,
		  authentication_form=AuthenticationForm,
		  current_app=None, extra_context=None):
	
	if request.user.is_authenticated():
		return HttpResponseRedirect('success/')
	
	return login(request, template_name, redirect_field_name,authentication_form, current_app, extra_context)


@login_required
def homeredirect(request):
	if(request.user.groups.filter(name='Alumno').count() > 0):
		return redirect(reverse('admin_coevaluaciones:ver_coevaluaciones_alumno'))
	elif(request.user.groups.filter(name='Profesor').count() > 0 or request.user.groups.filter(name='Ayudante').count() > 0):
		return redirect('admin_coevaluaciones:ver_coevaluaciones_profesor')
	else:
		return redirect('admin:index')
	
	
	