# -*- coding: utf-8 -*-
from ..models import Coevaluacion, Pregunta, Respuesta
from django.contrib import admin
from django.conf.urls import patterns
from django.contrib.admin.sites import AlreadyRegistered


def make_published(modeladmin, request, queryset):
    queryset.update(status='P')
    make_published.short_description = "Marcar para publicar"

class PreguntaSortable(admin.StackedInline):
    model = Pregunta
    extra = 0

class CoevaluacionAdmin(admin.ModelAdmin):
    list_display = ['__str__', 'status']
    inlines = [PreguntaSortable,]
    model = Coevaluacion
    actions = [make_published,]

class RespuestaAdmin(admin.ModelAdmin):
    list_display = ['__str__', 'estudiante_origen','estudiante_evaluado']

try:
    admin.site.register(Coevaluacion, CoevaluacionAdmin)
    admin.site.register(Respuesta, RespuestaAdmin)
except AlreadyRegistered:
    pass