# -*- coding: utf-8 -*-
from django.conf.urls import url, patterns
from django.contrib import admin
from django.contrib.auth.decorators import login_required, permission_required
from .views import SubirPreguntasCoevaluacion, DescarEjemploCoevaluacion,\
				CoevaluacionCrearViewProfesor, CoevaluacionListViewProfesor,\
				CoevaluacionEditarViewProfesor, CoevaluacionEditarPreguntasViewProfesor,\
				CoevaluacionImportExcelAlumno, PublicarCoevaluacionProfesor,\
                    CoevaluacionAgregarPreguntaViewProfesor

from .views_estudiante import ResponderCoevaluacionView,\
	ResultadosAlumnoView, CoevaluacionListViewAlumno

from coevaluaciones.admin.views_result_profesor import ResultadosProfesorView,\
	ResultadosParcialesProfesorView, ResultadosProfesorGrupoView,\
	ResultadosGrupoPreguntaAlumnoView, ResultadosExportarGrupoView,\
     ResultadosExportarCoevaluacionView
                    
urlpatterns = patterns('',
     #Estudiante
     url(r'^coevaluaciones/alumno/responder/(?P<coevaluacion_pk>\d+)/$', admin.site.admin_view(ResponderCoevaluacionView.as_view()), name='coevaluacion_responder'),
     url(r'^coevaluaciones/alumno/resultados/(?P<coevaluacion_pk>\d+)/$', admin.site.admin_view(ResultadosAlumnoView.as_view()), name='coevaluacion_resultados'),
     url(r'^coevaluaciones/alumno/ver/$', admin.site.admin_view(CoevaluacionListViewAlumno.as_view()), name='ver_coevaluaciones_alumno'),
     #Resultados profesor
     url(r'^coevaluaciones/profesor/resultados/(?P<coevaluacion_pk>\d+)/$', admin.site.admin_view(ResultadosProfesorView.as_view()), name='coevaluacion_resultados_profesor'),
     url(r'^coevaluaciones/profesor/resultados-parciales/(?P<coevaluacion_pk>\d+)/$', admin.site.admin_view(ResultadosParcialesProfesorView.as_view()), name='coevaluacion_resultados_parciales_profesor'),
     url(r'^coevaluaciones/profesor/resultados/(?P<coevaluacion_pk>\d+)/grupo/(?P<grupo_pk>\d+)/$', admin.site.admin_view(ResultadosProfesorGrupoView.as_view()), name='coevaluacion_resultados_profesor_grupo'),
     url(r'^coevaluaciones/profesor/resultados/(?P<coevaluacion_pk>\d+)/(?P<pregunta_pk>\d+)/(?P<alumno_pk>\d+)/$', admin.site.admin_view(ResultadosGrupoPreguntaAlumnoView.as_view()), name='coevaluacion_resultados_profesor_grupo_pregunta_alumno'),
     url(r'^coevaluaciones/profesor/resultados-descarga/(?P<coevaluacion_pk>\d+)/grupo/(?P<grupo_pk>\d+)/$', admin.site.admin_view(ResultadosExportarGrupoView.as_view()), name='coevaluacion_resultados_profesor_descargar_grupo'),
     url(r'^coevaluaciones/profesor/resultados-descarga/(?P<coevaluacion_pk>\d+)/$', admin.site.admin_view(ResultadosExportarCoevaluacionView.as_view()), name='coevaluacion_resultados_profesor_descargar'),

     url(r'^coevaluaciones/importar-coevaluacion/(?P<coevaluacion_pk>\d+)/$', admin.site.admin_view(CoevaluacionImportExcelAlumno.as_view()), name='coevaluacion_importar'),
     url(r'^coevaluaciones/profesor/coevaluacion/(?P<coevaluacion_pk>\d+)/descagar/ejemplo/$', admin.site.admin_view(DescarEjemploCoevaluacion.as_view()), name='coevaluacion_descarar_ejemplo'),
     url(r'^coevaluaciones/profesor/coevaluacion/(?P<coevaluacion_pk>\d+)/subir/preguntas/$', admin.site.admin_view(SubirPreguntasCoevaluacion.as_view()), name='coevaluacion_subir_preguntas'),
     
     
     url(r'^coevaluaciones/profesor/ver/$', admin.site.admin_view(CoevaluacionListViewProfesor.as_view()), name='ver_coevaluaciones_profesor'),

     url(r'^coevaluaciones/profesor/coevaluacion/crear/$', admin.site.admin_view(CoevaluacionCrearViewProfesor.as_view()), name='crear_coevaluacion_profesor'),
     url(r'^coevaluaciones/profesor/coevaluacion/(?P<coevaluacion_pk>\d+)/agregar/preguntas/$', admin.site.admin_view(CoevaluacionAgregarPreguntaViewProfesor.as_view()), name='agregar_pregunta_coevaluacion_profesor'),
     url(r'^coevaluaciones/profesor/coevaluacion/(?P<coevaluacion_pk>\d+)/editar/preguntas/$', admin.site.admin_view(CoevaluacionEditarPreguntasViewProfesor.as_view()), name='editar_preguntas_coevaluacion_profesor'),
     url(r'^coevaluaciones/profesor/coevaluacion/(?P<pk>\d+)/editar/$', admin.site.admin_view(CoevaluacionEditarViewProfesor.as_view()), name='editar_coevaluacion_profesor'),     
     url(r'^coevaluaciones/(?P<coevaluacion_pk>\d+)/profesor/publicar/$', admin.site.admin_view(PublicarCoevaluacionProfesor.as_view()), name='publicar_coevaluacion_profesor'),
   
    )