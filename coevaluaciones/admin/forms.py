# -*- coding: UTF-8 -*-
import os

from django.forms.forms import Form
from django.forms import ValidationError 
from django.forms.fields import CharField, ChoiceField, FileField, DateField,\
    BooleanField, DateTimeField
from django.forms.widgets import Textarea, RadioSelect, HiddenInput, RadioInput,\
    CheckboxInput, Select, HiddenInput
from django.forms.models import inlineformset_factory, ModelForm
from itertools import chain
from coevaluaciones.models import Coevaluacion, Respuesta, Pregunta,\
    BOOL_CHOICES
from form_utils.forms import BetterForm
from cursos.models import Estudiante, Grupo
from django.utils.html import conditional_escape
from django.utils.safestring import mark_safe
from django.utils.encoding import force_text


EVAL_CHOICES = (
    ('1', 'Nunca'),
    ('2', 'Con dificultad'),
    ('3', 'A veces'),
    ('4', 'Regularmente'),
    ('5', 'Siempre'),
)

EVAL_CHOICES_CLAVE = (
    ('1', 'Nulo'),
    ('2', 'Malo'),
    ('3', 'Aceptable'),
    ('4', 'Bueno'),
    ('5', 'Excelente'),
)

class CoevaluacionCreateForm(ModelForm):
    
    def __init__(self, *args, **kwargs):
        profesor_pk = kwargs.pop('profesor_pk', None)
        super(CoevaluacionCreateForm, self).__init__(*args, **kwargs)
        self.fields['curso'].queryset = self.fields['curso'].queryset.filter(profesor__pk=profesor_pk)
    class Meta:
        model = Coevaluacion
        exclude = ('status','position','fecha_limite','fecha_publicacion')

class CustomDateField(DateTimeField):
    def __init__(self, *args, **kwargs):
        kwargs.setdefault('input_formats', ('%d/%m/%Y %H:%M',))
        super(CustomDateField, self).__init__(*args, **kwargs)



class CoevaluacionUpdateForm(ModelForm):
    fecha_limite = CustomDateField()
    def __init__(self, *args, **kwargs):
        super(CoevaluacionUpdateForm, self).__init__(*args, **kwargs)
        
        if kwargs['instance'].status == 'C':
            self.fields['fecha_limite'] = None

        
    class Meta:
        model = Coevaluacion
        exclude = ('status','curso','position','fecha_publicacion')
        
class CoevaluacionPublicacionForm(ModelForm):
    fecha_limite = CustomDateField()
    mailearalumnos = BooleanField(label="Notificar por correo electronico a alumnos.", required=False,initial=True,widget=RadioSelect(choices=BOOL_CHOICES))

    class Meta:
        model = Coevaluacion
        exclude = ('nombre','status','curso','position','fecha_publicacion')


class EditarViewCoevaluacion(ModelForm):
    class Meta:
        model = Coevaluacion

class AgregarPreguntaCoevaluacion(ModelForm):
    def __init__(self, *args, **kwargs):
        super(AgregarPreguntaCoevaluacion, self).__init__(*args, **kwargs)
        self.fields['title'].label = 'Enunciado'
        self.fields['pregunta_clave'].label = '¿Es una pregunta clave?'

    class Meta:
        model = Pregunta
        exclude = ('position', 'coevaluacion')

class PreguntaForm(ModelForm):
    class Meta:
        model = Pregunta
        widgets = { 
            'position': HiddenInput(),
        }   

PreguntasFormSet = inlineformset_factory(Coevaluacion, Pregunta, extra=0, form=PreguntaForm)


class CoevaluacionRespuestaGrupoPreguntaForm(ModelForm):    
    def __init__(self, *args, **kwargs):
    	grupo = kwargs.pop('grupo', None)
        estudiante_origen = kwargs.pop('estudiante_origen', None)
        pregunta = kwargs.pop('pregunta', None) 
        estudiante_evaluado = kwargs.pop('estudiante_evaluado', None) 
        super(CoevaluacionRespuestaGrupoPreguntaForm, self).__init__(*args, **kwargs)
        self.fields['pregunta'].widget = HiddenInput()
        self.fields['pregunta'].initial = pregunta
        self.fields['estudiante_origen'].widget = HiddenInput()
        self.fields['estudiante_origen'].initial = estudiante_origen
        self.fields['estudiante_evaluado'].widget = HiddenInput()
        self.fields['estudiante_evaluado'].initial = estudiante_evaluado
        #self.fields['respuesta_eval'] = HiddenInput()
        #self.fields['respuesta_text'] = HiddenInput()
        self.fields['grupo'].widget = HiddenInput()
        self.fields['grupo'].initial = grupo

        if pregunta.tipo  == 'eval':
            if pregunta.pregunta_clave:
                self.fields['respuesta_eval'] = ChoiceField(choices=EVAL_CHOICES_CLAVE, required=False, widget=RadioSelect(attrs={'class':'radio inline'}))
            else:
                self.fields['respuesta_eval'] = ChoiceField(choices=EVAL_CHOICES, required=False, widget=RadioSelect(attrs={'class':'radio inline'}))
            self.fields['respuesta_eval'].label ="%s %s" %(estudiante_evaluado.first_name, estudiante_evaluado.last_name)
        elif pregunta.tipo == 'txt': 
            self.fields['respuesta_text'] = CharField(widget=Textarea, required=False)
            self.fields['respuesta_text'].label ="%s %s" %(estudiante_evaluado.first_name, estudiante_evaluado.last_name)
        
    class Meta:
        model = Respuesta

IMPORT_FILE_TYPES = ['.xls','.xlsx' ]

class XlsInputForm(Form):
    input_excel = FileField(required= True, label= u"Importar Coevaluaciones mediante un excel.")

    def clean_input_excel(self):
        input_excel = self.cleaned_data['input_excel']
        extension = os.path.splitext( input_excel.name )[1]
        if not (extension in IMPORT_FILE_TYPES):
            raise ValidationError( u'%s No es un archivo válido' % extension )
        else:
            return input_excel