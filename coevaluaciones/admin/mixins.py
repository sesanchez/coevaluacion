from django.utils.decorators import method_decorator
from django.contrib.auth.decorators import permission_required


class ProfesorMixin(object):   
    @method_decorator(permission_required('coevaluaciones.profesor'))
    def dispatch(self, *args, **kwargs):
        return super(ProfesorMixin, self).dispatch(*args, **kwargs)
    
    def get_context_data(self, **kwargs):
        context = {}
        context['usuario'] = self.request.user
        context['rol'] = 'profesor'
        context.update(kwargs)
        return super(ProfesorMixin, self).get_context_data(**context)
    
    
class EstudianteMixin(object):

    @method_decorator(permission_required('coevaluaciones.alumno'))
    def dispatch(self, *args, **kwargs):
        return super(EstudianteMixin, self).dispatch(*args, **kwargs)
    
    def get_context_data(self, **kwargs):
        context = {}
        context['usuario'] = self.request.user
        context['rol'] = 'estudiante'
        context.update(kwargs)
        return super(EstudianteMixin, self).get_context_data(**context)