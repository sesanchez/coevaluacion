from django.test.client import Client
from django.test.testcases import TestCase, TransactionTestCase
from django.utils.translation import ugettext as _
from django.core.urlresolvers import reverse
from django.contrib.auth.forms import AuthenticationForm
from cursos.models import Profesor, Estudiante, Curso
from django.contrib.auth.models import Group, User
from django.core import mail
from cursos.tests.utils import CursoMixin
from coevaluaciones.models import Coevaluacion



class ViewCoevaluacionTestConCursoAlumno(TransactionTestCase, CursoMixin):
    fixtures = ['fixtures/group']
    
    def setUp(self):
        self.user = Estudiante.objects.create_user(username='testalumno', password='testalumno')
        self.user.is_active = True
        self.user.is_staff = True
        group = Group.objects.get(name='Alumno') 
        self.user.groups.add(group)
        self.user.save()
        self.profesor = self.create_profesor()
        self.curso = self.create_curso()
        
        for i in range(1,4):
            self.create_alumno()
            
        self.asignar_alumno(self.user)
        
        for i in range(1,2):
            self.create_grupo()

        self.validar_curso()

        
        self.url = reverse('admin_coevaluaciones:ver_coevaluaciones_alumno')
        #expected_url = reverse('admin_coevaluaciones:ver_coevaluaciones_profesor')    
        self.c = Client()

        self.c.login(username='testalumno', password='testalumno')


    def tearDown(self):
        self.user.delete()
        self.clear()

    def test_coevaluaciones_sin_coevaluacion(self):
        response = self.c.get(self.url,follow=True)
        
        self.assertEqual(response.status_code, 200)
        self.assertIn('coevaluaciones',response.context)
        self.assertEqual(len(response.context['coevaluaciones']),0)
    
    def test_coevaluaciones_con_coevaluaciones(self):
        coev = Coevaluacion(nombre="test_coevaluacion", position=0, curso = self.curso )
        coev.save()
        response = self.c.get(self.url,follow=True)
        self.assertEqual(response.status_code, 200)
        
        self.assertIn('coevaluaciones',response.context)
        self.assertEqual(len(response.context['coevaluaciones']),1)
        coev.delete()



