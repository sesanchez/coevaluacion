# -*- coding: UTF-8 -*-
from django.http import Http404
from django.views.generic.base import TemplateView
from django.utils.decorators import method_decorator
from django.contrib.auth.decorators import permission_required
from coevaluaciones.admin.mixins import ProfesorMixin
from coevaluaciones.models import Coevaluacion, Pregunta
from cursos.models import Grupo, Estudiante
from library.views.views import BajarExcelView

import xlwt
  

class ResultadosProfesorView(ProfesorMixin, TemplateView):
    template_name = 'admin/coevaluaciones/profesor/resultados/resultados.html'
    
    def get_coevaluacion(self, prefesor_pk, coevaluacion_pk):
        try:
            return Coevaluacion.objects.filter(status='F', curso__profesor=prefesor_pk ).get(pk=coevaluacion_pk)
        except Coevaluacion.DoesNotExist:
            raise Http404
        
    def get_context_data(self, **kwargs):
        kwargs = super(ResultadosProfesorView, self).get_context_data(**kwargs)
        kwargs['coevaluacion'] = self.get_coevaluacion(self.request.user.pk, kwargs.pop("coevaluacion_pk",None))

        return kwargs

class ResultadosParcialesProfesorView(ResultadosProfesorView):
    template_name = 'admin/coevaluaciones/profesor/resultados/resultados_parciales.html'
    
    def get_coevaluacion(self, prefesor_pk, coevaluacion_pk):
        try:
            return Coevaluacion.objects.filter(curso__profesor=prefesor_pk ).get(pk=coevaluacion_pk)
        except Coevaluacion.DoesNotExist:
            raise Http404


class ResultadosProfesorGrupoView(ProfesorMixin, TemplateView):
    template_name = 'admin/coevaluaciones/profesor/resultados/porgrupo_view.html'

    def get_context_data(self, **kwargs):
        kwargs = super(ResultadosProfesorGrupoView, self).get_context_data(**kwargs)
        try:
            coevaluacion_pk = kwargs.pop("coevaluacion_pk",None)
            grupo_pk = kwargs.pop("grupo_pk",None)
            kwargs['coevaluacion'] = Coevaluacion.objects.filter(curso__profesor= self.request.user.pk, curso__grupo =grupo_pk).get(pk=coevaluacion_pk)
            grupo = Grupo.objects.get(pk=grupo_pk)
            kwargs['alumnos'] = grupo.alumnos
            kwargs['grupo'] = grupo.alumnos
        except Coevaluacion.DoesNotExist:
            raise Http404
        except Grupo.DoesNotExist:
            raise Http404

        return kwargs

class ResultadosGrupoPreguntaAlumnoView(ProfesorMixin, TemplateView):
    template_name = 'admin/coevaluaciones/profesor/resultados/porpregunta_alumno_view.html'

    def get_context_data(self, **kwargs):
        kwargs = super(ResultadosGrupoPreguntaAlumnoView, self).get_context_data(**kwargs)
        try:
            coevaluacion_pk = kwargs.pop("coevaluacion_pk",None)
            pregunta_pk = kwargs.pop("pregunta_pk",None)
            alumno_pk = kwargs.pop("alumno_pk",None)
            coevaluacion = Coevaluacion.objects.filter(curso__profesor= self.request.user.pk).get(pk=coevaluacion_pk)
            pregunta = Pregunta.objects.get(pk=pregunta_pk)
            alumno = Estudiante.objects.get(pk=alumno_pk)
        except Coevaluacion.DoesNotExist:
            raise Http404
        except Pregunta.DoesNotExist:
            raise Http404
        except Estudiante.DoesNotExist:
            raise Http404
        
        respuestas = pregunta.respuesta_set.filter(estudiante_evaluado = alumno)
        kwargs['coevaluacion'] = coevaluacion
        kwargs['alumno'] = alumno
        kwargs['pregunta'] = pregunta
        kwargs['respuestas'] = respuestas
        return kwargs

class ResultadosExportarGrupoView(ProfesorMixin, BajarExcelView):
    def get_book(self):
        book = xlwt.Workbook(encoding='utf8')
        sheet = book.add_sheet('preguntas')  
        
        try:
            coevaluacion_pk = self.kwargs.pop("coevaluacion_pk",None)
            grupo_pk = self.kwargs.pop("grupo_pk",None)
            coevaluacion = Coevaluacion.objects.filter(curso__profesor= self.request.user.pk, curso__grupo =grupo_pk).get(pk=coevaluacion_pk)
            grupo = Grupo.objects.get(pk=grupo_pk)
            alumnos = grupo.alumnos
        except Coevaluacion.DoesNotExist:
            raise Http404
        except Grupo.DoesNotExist:
            raise Http404
        # Adding style for cell
        # Create Alignment
        alignment = xlwt.Alignment()
        # horz May be: HORZ_GENERAL, HORZ_LEFT, HORZ_CENTER, HORZ_RIGHT,    
        # HORZ_FILLED, HORZ_JUSTIFIED, HORZ_CENTER_ACROSS_SEL,
        # HORZ_DISTRIBUTED
        alignment.horz = xlwt.Alignment.HORZ_LEFT
        # May be: VERT_TOP, VERT_CENTER, VERT_BOTTOM, VERT_JUSTIFIED,
        # VERT_DISTRIBUTED
        alignment.vert = xlwt.Alignment.VERT_TOP
        col_width = 256 * 10                        # 20 characters wide
        try:
            sheet.col(0).width = 256 * 15
            sheet.col(1).width = 256 * 15
            sheet.col(2).width = 256 * 35
            sheet.col(3).width = 256 * 35
            sheet.col(4).width = 256 * 35
            sheet.col(5).width = 256 * 35
        except ValueError:
            pass
        style = xlwt.XFStyle() # Create Style
        style.alignment = alignment # Add Alignment to Style
        # write the header
        curso = coevaluacion.curso
        row = 1
        legend_row = ['Curso', 'Semestre', 'Profesor', u'Coevaluacón', 'Equipo', 'Evaluador', 'Evaluado']
        legend_row = legend_row + [pregunta.title for pregunta in coevaluacion.pregunta_set.all()]
        for i, cell in enumerate(legend_row):
                    sheet.write(row, i, cell, style=xlwt.Style.default_style)
        row = 2
        for evaluador in grupo.alumnos.all():
            for evaluado in grupo.alumnos.exclude(pk=evaluador.pk):
                data_row = [ curso.nombre,
                             curso.semestre,
                             "%s %s"%(curso.profesor.first_name, curso.profesor.last_name),
                             grupo.nombre,
                             coevaluacion.nombre,
                             "%s %s"%(evaluador.first_name, evaluador.last_name),#origin
                             "%s %s"%(evaluado.first_name, evaluado.last_name),#evaluado
                            ]
                data_row = data_row + [respuesta.get_respuesta() for respuesta in evaluador.estudiante_responde.filter(pregunta__coevaluacion = coevaluacion,estudiante_origen=evaluador, estudiante_evaluado=evaluado)]
                for i, cell in enumerate(data_row):
                    sheet.write(row, i, cell, style=xlwt.Style.default_style)
                row = row+1
                
        return book

class ResultadosExportarCoevaluacionView(ProfesorMixin, BajarExcelView):
    def get_book(self):
        book = xlwt.Workbook(encoding='utf8')
        sheet = book.add_sheet('preguntas')  
        
        try:
            coevaluacion_pk = self.kwargs.pop("coevaluacion_pk",None)
            coevaluacion = Coevaluacion.objects.filter(curso__profesor= self.request.user.pk).get(pk=coevaluacion_pk)
        except Coevaluacion.DoesNotExist:
            raise Http404
        # Adding style for cell
        # Create Alignment
        alignment = xlwt.Alignment()
        # horz May be: HORZ_GENERAL, HORZ_LEFT, HORZ_CENTER, HORZ_RIGHT,    
        # HORZ_FILLED, HORZ_JUSTIFIED, HORZ_CENTER_ACROSS_SEL,
        # HORZ_DISTRIBUTED
        alignment.horz = xlwt.Alignment.HORZ_LEFT
        # May be: VERT_TOP, VERT_CENTER, VERT_BOTTOM, VERT_JUSTIFIED,
        # VERT_DISTRIBUTED
        alignment.vert = xlwt.Alignment.VERT_TOP
        col_width = 256 * 10                        # 20 characters wide
        try:
            sheet.col(0).width = 256 * 15
            sheet.col(1).width = 256 * 15
            sheet.col(2).width = 256 * 35
            sheet.col(3).width = 256 * 15
            sheet.col(4).width = 256 * 35
            sheet.col(5).width = 256 * 35
            sheet.col(6).width = 256 * 35
        except ValueError:
            pass
        style = xlwt.XFStyle() # Create Style
        style.alignment = alignment # Add Alignment to Style
        # write the header
        curso = coevaluacion.curso
        row = 1
        legend_row = ['Curso','Semestre', 'Profesor',u'Coevaluación ','Equipo', 'Evaluador', 'Evaluado']
        legend_row = legend_row + [pregunta.title for pregunta in coevaluacion.pregunta_set.all()]
        for i, cell in enumerate(legend_row):
                    sheet.write(row, i, cell, style=xlwt.Style.default_style)
        row = 2
        for grupo in curso.grupo_set.all():
            for evaluador in grupo.alumnos.all():
                for evaluado in grupo.alumnos.exclude(pk=evaluador.pk):
                    data_row = [ curso.nombre,
                                 curso.semestre,
                                 "%s %s"%(curso.profesor.first_name, curso.profesor.last_name),
                                 coevaluacion.nombre,
                                 grupo.nombre,
                                 "%s %s"%(evaluador.first_name, evaluador.last_name),#origin
                                 "%s %s"%(evaluado.first_name, evaluado.last_name),#evaluado
                                ]
                    data_row = data_row + [respuesta.get_respuesta() for respuesta in evaluador.estudiante_responde.filter(pregunta__coevaluacion = coevaluacion,estudiante_origen=evaluador, estudiante_evaluado=evaluado)]
                    for i, cell in enumerate(data_row):
                        sheet.write(row, i, cell, style=xlwt.Style.default_style)
                    row = row+1
                
        return book