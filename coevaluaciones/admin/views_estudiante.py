# -*- coding: UTF-8 -*-
from django.contrib import messages
from django.http import Http404
from django.shortcuts import redirect
from django.views.generic.base import TemplateView
from django.utils.decorators import method_decorator
from django.contrib.auth.decorators import permission_required
from coevaluaciones.admin.forms import CoevaluacionRespuestaGrupoPreguntaForm
from coevaluaciones.models import Coevaluacion, Respuesta
from cursos.models import Estudiante


class CoevaluacionListViewAlumno(TemplateView):
    template_name = 'admin/coevaluaciones/coevaluaciones_list_view.html'
    
    
    @method_decorator(permission_required('coevaluaciones.alumno'))
    def dispatch(self, *args, **kwargs):
        return super(CoevaluacionListViewAlumno, self).dispatch(*args, **kwargs)
    
    def get_context_data(self, **kwargs):
        kwargs = super(CoevaluacionListViewAlumno, self).get_context_data(**kwargs)
        coevaluaciones = Coevaluacion.objects.exclude(status='C').filter(curso__grupo__alumnos__pk = self.request.user.pk).order_by('-fecha_publicacion')
        kwargs['coevaluaciones'] = coevaluaciones
        kwargs['usuario'] = self.request.user
        kwargs['rol'] = 'estudiante'
        return kwargs

class ResultadosAlumnoView(TemplateView):
    template_name = 'admin/coevaluaciones/coevaluaciones_alumno_result_view.html'
    
    @method_decorator(permission_required('coevaluaciones.alumno'))
    def dispatch(self, *args, **kwargs):
        return super(ResultadosAlumnoView, self).dispatch(*args, **kwargs)
    
    def get_context_data(self, **kwargs):
        kwargs = super(ResultadosAlumnoView, self).get_context_data(**kwargs)
        try:
            coevaluacion = Coevaluacion.objects.filter(status='F', curso__grupo__alumnos__pk = self.request.user.pk).get(pk= kwargs.pop("coevaluacion_pk",None))
        except Coevaluacion.DoesNotExist:
            raise Http404
        kwargs['coevaluacion'] = coevaluacion
        kwargs['usuario'] = self.request.user
        kwargs['rol'] = 'estudiante'
        
        try:
            estudiante = Estudiante.objects.get(pk = self.request.user.pk)#get by session
            kwargs['estudiante'] = estudiante
        except Estudiante.DoesNotExist:
            raise Http404
        if estudiante:
            kwargs['grupo'] = coevaluacion.curso.get_grupo_by_alumno(estudiante)
              
            #preguntas = [pk:{'pregunta':'pregunta 1', 'promedio': '3', respuestas:[]})]
            preguntas = []
            for pregunta in coevaluacion.pregunta_set.all():
                promedio = '-'
                if pregunta.tipo == 'eval':
                    respuestas = pregunta.respuesta_set.filter(estudiante_evaluado=estudiante, respuesta_eval__isnull=False )
                    promedio = 0
                    for respuesta in respuestas:
                            promedio += respuesta.respuesta_eval
                    try:
                        promedio  = '{0:.2g}'.format(float(promedio) / respuestas.count()) 
                    except:
                        promedio = '-'
                else:
                    respuestas = pregunta.respuesta_set.filter(estudiante_evaluado=estudiante, respuesta_text__isnull=False )

                    
                preguntas.append({'pregunta': pregunta, 'promedio': promedio, 'respuestas':respuestas})                     
        
        kwargs['preguntas']  = preguntas   
        return kwargs


class ResponderCoevaluacionView(TemplateView):
    template_name = 'admin/coevaluaciones/create_respuesta.html'
    
    @method_decorator(permission_required('coevaluaciones.alumno'))
    def dispatch(self, *args, **kwargs):
        return super(ResponderCoevaluacionView, self).dispatch(*args, **kwargs)
    
    def post(self, request, *args, **kwargs):
        context = self.get_context_data(**kwargs)
        #se obtienen los formularios y se verifica que sean todos validos
        if False not in [False not in [respuesta_alumno.is_valid() for id,respuesta_alumno in respuestas_alumnos['alumnos'].items()] for respuestas_alumnos in context['preguntas']]:
            #se obtienen los formularios y se verifica que sean todos validos
            esta_actualizando = context['coevaluacion'].alumno_contestada(context['estudiante'])
            alumnos_en_grupo = context['grupo'].alumnos.exclude(pk=context['estudiante'].pk)

            for preguntas in context['preguntas']:
                for id, alumno in preguntas['alumnos'].items():
                    respuesta = alumno.save(commit=False)
                    if respuesta.estudiante_evaluado in alumnos_en_grupo and (respuesta.pregunta.tipo == 'txt' or (respuesta.respuesta_eval and respuesta.respuesta_eval <= 5)):
                        respuesta.save()
            if esta_actualizando:
                messages.success(request, 'Coevaluación actualizada.')
            else:
                messages.success(request, 'Coevaluación respondida con éxito.')   
            return redirect('admin_coevaluaciones:ver_coevaluaciones_alumno')
        else:
            return self.render_to_response(context)

    def get_context_data(self, **kwargs):
        form_kwargs = {}
        kwargs = super(ResponderCoevaluacionView, self).get_context_data(**kwargs)
        try:
            coevaluacion = Coevaluacion.objects.filter(status='P').get(pk= kwargs.pop("coevaluacion_pk",None))
        except Coevaluacion.DoesNotExist:
            raise Http404
        try:
            estudiante = coevaluacion.curso.estudiante_set.get(pk = self.request.user.pk)#get by session
            kwargs['estudiante'] = estudiante
        except Estudiante.DoesNotExist:
            raise Http404

        grupo = coevaluacion.curso.get_grupo_by_alumno(estudiante)
        if grupo:
            form_kwargs['grupo'] = grupo
            form_kwargs['estudiante_origen'] = estudiante
            preguntas = []
            if self.request.method == 'POST':
                form_kwargs['data'] = self.request.POST
            # Para cada Pregunta y para cada estudiante a cual evaluar se crea un formulario "CoevaluacionRespuestaGrupoPreguntaForm"
            for pregunta in coevaluacion.pregunta_set.all():
                form_kwargs['pregunta'] = pregunta
                alumnos = {}
                for alumno in grupo.alumnos.exclude(pk=estudiante.pk):
                    if coevaluacion.alumno_contestada(estudiante):
                        try:
                            form_kwargs['instance'] = pregunta.respuesta_set.get(estudiante_origen__pk=estudiante.pk, estudiante_evaluado__pk = alumno.pk)
                        except Respuesta.DoesNotExist:
                            form_kwargs['instance'] = None
                    form_kwargs['estudiante_evaluado'] = alumno
                    if alumno != estudiante:#obtengo el formulario por cada pregunta y alumno
                        alumnos[alumno.pk]= CoevaluacionRespuestaGrupoPreguntaForm(prefix='pregunta_%s__alumno_%s'%(pregunta.pk, alumno.pk), **form_kwargs)
                preguntas.append({'pregunta':pregunta, 'alumnos':alumnos})
            kwargs['preguntas'] = preguntas
        kwargs['grupo'] = grupo
        kwargs['estudiante'] = estudiante
        kwargs['usuario'] = self.request.user
        kwargs['rol']='estudiante'
        kwargs['coevaluacion'] = coevaluacion
        return kwargs