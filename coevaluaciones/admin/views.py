# -*- coding: UTF-8 -*-
from django.db.models import Q
import xlrd
import xlwt

from django.views.generic.base import TemplateView
from django.views.generic.edit import FormView, CreateView, UpdateView
from django.core.urlresolvers import reverse

from django.contrib import messages

from coevaluaciones.models import Coevaluacion, Pregunta, Respuesta
from cursos.models import Grupo, Estudiante
from coevaluaciones.admin.forms import XlsInputForm, PreguntasFormSet, CoevaluacionUpdateForm,\
    CoevaluacionCreateForm, CoevaluacionPublicacionForm, AgregarPreguntaCoevaluacion
    
from django.http.response import HttpResponseRedirect, Http404
from django.db.models.aggregates import Max
import datetime
from django.template.loader import get_template
from django.template.context import Context
from django.core.mail.message import EmailMessage
from django.contrib.sites.models import Site
from django.conf import settings
from mixins import ProfesorMixin
from library.views.views import BajarExcelView


class CoevaluacionListViewProfesor(ProfesorMixin, TemplateView):
    template_name = 'admin/coevaluaciones/profesor/listar.html'
    
    def get_context_data(self, **kwargs):
        kwargs = super(CoevaluacionListViewProfesor, self).get_context_data(**kwargs)
        coevaluaciones = Coevaluacion.objects.filter(curso__profesor__pk = self.request.user.pk).order_by('-fecha_publicacion')
        kwargs['coevaluaciones'] = coevaluaciones
        return kwargs


class PublicarCoevaluacionProfesor(ProfesorMixin, UpdateView):
    template_name = 'admin/coevaluaciones/coevaluacion_publicacion_profesor.html'
    form_class=CoevaluacionPublicacionForm
    model = Coevaluacion
    pk_url_kwarg = 'coevaluacion_pk'
    
    def get_context_data(self, **kwargs):
        kwargs = super(PublicarCoevaluacionProfesor, self).get_context_data(**kwargs)
        try:
            coevaluacion = Coevaluacion.objects.get(pk= self.kwargs.pop("coevaluacion_pk",None))
        except Coevaluacion.DoesNotExist:
            raise Http404
        kwargs['coevaluacion'] = coevaluacion
        
        return kwargs
   
    def form_valid(self,form):
        coevaluacion = form.save(commit=False)
        coevaluacion.status = "P"
        coevaluacion.fecha_publicacion = datetime.date.today()
        coevaluacion.save()
        alumnos = coevaluacion.curso.estudiante_set.all()
        
        current_site = Site.objects.get_current()
        context = {'coevaluacion' : coevaluacion,
                   'curso' : coevaluacion.curso,
                   'current_site': current_site
                    }
        
        for alumno in alumnos:
            context['alumno'] = alumno
            template = get_template('mail/mail_publicacion.html')
            alumno_context = Context(context)
            template_alumno = template.render(alumno_context)
            msg = EmailMessage('Responder coevaluación', template_alumno, settings.SERVER_EMAIL, [alumno.email])
            msg.content_subtype = "html"  # Main content is now text/html
            msg.send(fail_silently=False)
        
        messages.success(self.request, 'Coevaluación Publicada.')
        
        return HttpResponseRedirect(reverse('admin_coevaluaciones:ver_coevaluaciones_profesor'))

class CoevaluacionEditarViewProfesor(ProfesorMixin, UpdateView):
    template_name = 'admin/coevaluaciones/coevaluacion_edit_view.html'
    model = Coevaluacion
    form_class=CoevaluacionUpdateForm
       
    def get_context_data(self, **kwargs):
        kwargs = super(CoevaluacionEditarViewProfesor, self).get_context_data(**kwargs)
        kwargs['coevaluaciones'] = Coevaluacion.objects.filter(curso=self.object.curso).exclude(pk=self.object.pk)
        kwargs['coevaluacion'] = self.object
        return kwargs

    def form_valid(self, form):
        coevaluacion = form.save(commit=False)
        if coevaluacion.fecha_limite > datetime.datetime.now():
            coevaluacion.status = "P"
        coevaluacion.save()
        return HttpResponseRedirect(self.get_success_url())

    def get_success_url(self):
        messages.success(self.request, 'Coevaluación actualizada.')
        return reverse('admin_coevaluaciones:ver_coevaluaciones_profesor')

class CoevaluacionAgregarPreguntaViewProfesor(ProfesorMixin, FormView):
    template_name = 'admin/coevaluaciones/profesor/preguntas/agregar.html'
    form_class = AgregarPreguntaCoevaluacion

    def get_context_data(self, **kwargs):
        kwargs = super(CoevaluacionAgregarPreguntaViewProfesor, self).get_context_data(**kwargs)
        try:
            coevaluacion = Coevaluacion.objects.get(curso__profesor__pk=self.request.user.pk, pk= self.kwargs.pop("coevaluacion_pk",None))
        except Coevaluacion.DoesNotExist:
            raise Http404
        kwargs['coevaluacion'] = coevaluacion
        return kwargs

    def form_valid(self, form):
        pregunta = form.save(commit=False)
        try:
            coevaluacion = Coevaluacion.objects.get(curso__profesor__pk=self.request.user.pk, pk= self.kwargs.pop("coevaluacion_pk",None))
        except Coevaluacion.DoesNotExist:
            raise Http404
        pregunta.coevaluacion  = coevaluacion
        preguntas = coevaluacion.pregunta_set.aggregate(Max('position'))
        if preguntas['position__max']:
            pregunta.position = preguntas['position__max']+1
        else:
            pregunta.position = 0
        pregunta.save()
        messages.success(self.request, 'pregunta agregada.')
        return HttpResponseRedirect(reverse('admin_coevaluaciones:editar_preguntas_coevaluacion_profesor', kwargs={'coevaluacion_pk':coevaluacion.pk}))


class CoevaluacionEditarPreguntasViewProfesor(ProfesorMixin, TemplateView):
    template_name = 'admin/coevaluaciones/profesor/preguntas/editar.html'
    model = Coevaluacion
    
    def get_context_data(self, **kwargs):
        kwargs = super(CoevaluacionEditarPreguntasViewProfesor, self).get_context_data(**kwargs)
        try:
            coevaluacion = Coevaluacion.objects.get(pk= self.kwargs.pop("coevaluacion_pk",None))
        except Coevaluacion.DoesNotExist:
            raise Http404
        
        if self.request.POST:
            kwargs['preguntas'] = PreguntasFormSet(self.request.POST, instance=coevaluacion)
        else:
            kwargs['preguntas'] = PreguntasFormSet(instance=coevaluacion)

        kwargs['coevaluacion'] = coevaluacion
        return kwargs

    def post(self, request, *args, **kwargs):
        context = self.get_context_data(**kwargs)
        if context['preguntas'].is_valid():
            for pregunta in context['preguntas'].forms:
                pregunta_to_save = pregunta.save(commit=False)
                if pregunta_to_save.title:
                    pregunta_to_save.save()
                else:
                    pregunta_to_save.delete()
            
            messages.success(self.request, 'Coevaluación editada.')
            return HttpResponseRedirect(reverse('admin_coevaluaciones:ver_coevaluaciones_profesor'))
        else:
            return self.render_to_response(self.get_context_data(preguntas=context['preguntas']))



class CoevaluacionCrearViewProfesor(ProfesorMixin, CreateView):
    template_name = 'admin/coevaluaciones/coevaluacion_create_view.html'
    model = Coevaluacion
    form_class = CoevaluacionCreateForm
        
    def get_form_kwargs(self):
        kwargs = super(CoevaluacionCrearViewProfesor, self).get_form_kwargs()
        kwargs['profesor_pk'] = self.request.user.pk
        return kwargs

    def get_context_data(self, **kwargs):
        kwargs = super(CoevaluacionCrearViewProfesor, self).get_context_data(**kwargs)
        
        kwargs['profesor'] = self.request.user
        try:
            coevaluacion = Coevaluacion.objects.get(pk= self.kwargs.pop("coevaluacion_pk",None))
        except Coevaluacion.DoesNotExist:
            coevaluacion=None
        kwargs['coevaluacion'] = coevaluacion
        return kwargs
    
    def form_valid(self,form):
        coevaluacion = form.save(commit=False)
        coevaluaciones = Coevaluacion.objects.filter(curso=coevaluacion.curso).aggregate(Max('position'))
        if coevaluaciones['position__max']:
            coevaluacion.position = coevaluaciones['position__max']+1
        else:
            coevaluacion.position = 1
        coevaluacion.status = "C"
        coevaluacion.save()
        messages.success(self.request, 'Coevaluación Creada.')
        return HttpResponseRedirect(reverse('admin_coevaluaciones:ver_coevaluaciones_profesor'))


class SubirPreguntasCoevaluacion(ProfesorMixin, FormView):
    template_name = 'admin/coevaluaciones/profesor/preguntas/importar.html'
    form_class = XlsInputForm
    
    def get_context_data(self, **kwargs):
        kwargs = super(SubirPreguntasCoevaluacion, self).get_context_data(**kwargs)
        try:
            coevaluacion = Coevaluacion.objects.get(pk= self.kwargs.pop("coevaluacion_pk",None))
        except Coevaluacion.DoesNotExist:
            raise Http404 
        kwargs['coevaluacion'] = coevaluacion
        return kwargs

    def form_valid(self, form):

        try:
            coevaluacion = Coevaluacion.objects.get(pk= self.kwargs.pop("coevaluacion_pk",None))
        except Coevaluacion.DoesNotExist:
            raise Http404

        if coevaluacion:
            input_excel = form.cleaned_data['input_excel']
            coevaluacion_excel = xlrd.open_workbook(file_contents=input_excel.read())
            coevaluacion_hoja = coevaluacion_excel.sheet_by_index(0)
            #leo el documento
            #obtengo la primera Hoja
            # en la primera fila
            # for row in range(1, coevaluacion_hoja.nrows): 
            preguntas = []
            errores_en_pregunta = 0
            for pregunta_row in range(5, coevaluacion_hoja.nrows):
                if coevaluacion_hoja.cell_value(pregunta_row, 0) and coevaluacion_hoja.cell_value(pregunta_row, 1):
                    try:
                        tipo =  'txt' if coevaluacion_hoja.cell_value(pregunta_row, 1) == 'txt'.lower() else 'eval'
                        clave =  bool(coevaluacion_hoja.cell_value(pregunta_row, 2)) and 'clave' == coevaluacion_hoja.cell_value(pregunta_row, 2)
                        pregunta_args = {
                            'position': pregunta_row-5,
                            'title': coevaluacion_hoja.cell_value(pregunta_row, 0),
                            'tipo': tipo,
                            'coevaluacion': coevaluacion
                        }
                        pregunta  = Pregunta(**pregunta_args)
                        preguntas.append(pregunta)
                    except:
                        errores_en_pregunta = errores_en_pregunta+1

            Pregunta.objects.filter(coevaluacion=coevaluacion).delete()
            coevaluacion.pregunta_set.add(*preguntas)
            coevaluacion.save()
        if errores_en_pregunta != 0:
            messages.warning(self.request, 'Preguntas con errores. %d'% 0)

        messages.success(self.request, 'Preguntas subidas. %d' % len(preguntas))
        return HttpResponseRedirect(reverse('admin_coevaluaciones:ver_coevaluaciones_profesor'))
        

class DescarEjemploCoevaluacion(ProfesorMixin, BajarExcelView):

    def get_book(self):
        book = xlwt.Workbook(encoding='utf8')
        sheet = book.add_sheet('preguntas')  

        coevaluacion_pk = self.kwargs.get('coevaluacion_pk', None)
        coevaluacion = Coevaluacion.objects.filter(curso__profesor__pk = self.request.user.pk).get(pk=coevaluacion_pk)
        # Adding style for cell
        # Create Alignment
        alignment = xlwt.Alignment()
        # horz May be: HORZ_GENERAL, HORZ_LEFT, HORZ_CENTER, HORZ_RIGHT,    
        # HORZ_FILLED, HORZ_JUSTIFIED, HORZ_CENTER_ACROSS_SEL,
        # HORZ_DISTRIBUTED
        alignment.horz = xlwt.Alignment.HORZ_LEFT
        # May be: VERT_TOP, VERT_CENTER, VERT_BOTTOM, VERT_JUSTIFIED,
        # VERT_DISTRIBUTED
        alignment.vert = xlwt.Alignment.VERT_TOP
        col_width = 256 * 10                        # 20 characters wide
        try:
            sheet.col(0).width = 256 * 60
            sheet.col(1).width = 256 * 15
        except ValueError:
            pass
        style = xlwt.XFStyle() # Create Style
        style.alignment = alignment # Add Alignment to Style
        # write the header
        curso = coevaluacion.curso

        header = ['Curso:', curso.nombre]
        for hcol, hcol_data in enumerate(header): # [(0,'Header 1'), (1, 'Header 2'), (2,'Header 3'), (3,'Header 4')]
           sheet.write(0, hcol, hcol_data, style=xlwt.Style.default_style)
        header = ['Profesor:', "%s %s"%(curso.profesor.first_name, curso.profesor.last_name)]
        for hcol, hcol_data in enumerate(header): # [(0,'Header 1'), (1, 'Header 2'), (2,'Header 3'), (3,'Header 4')]
           sheet.write(1, hcol, hcol_data, style=xlwt.Style.default_style)
        
        header = ['Semestre:', "%s"%(curso.semestre)]
        for hcol, hcol_data in enumerate(header): # [(0,'Header 1'), (1, 'Header 2'), (2,'Header 3'), (3,'Header 4')]
           sheet.write(2, hcol, hcol_data, style=xlwt.Style.default_style)


        header = ['Pregunta', 'Tipo(eval/txt)', 'clave?']
        for hcol, hcol_data in enumerate(header): # [(0,'Header 1'), (1, 'Header 2'), (2,'Header 3'), (3,'Header 4')]
           sheet.write(4, hcol, hcol_data, style=xlwt.Style.default_style)

        
        # write your data, you can also get it from your model
        if coevaluacion.pregunta_set.count() == 0 : 
            data = [
                    ['Cumple de manera adecuada con las tareas que le son asignadas','eval'],
                    ['Demuestra compromiso con el proyecto','eval'],
                    ['Demuestra iniciativa para lograr el éxito del proyecto','eval'],
                    ['Mantiene buena comunicación con el resto del equipo','eval'],
                    ['Mantiene buena coordinación entre sus tareas y las de sus pares','eval'],
                    ['La calidad de su trabajo es la apropiada para lograr el éxito del proyecto','eval'],
                    ['Ofrece apoyo en tareas que van más allá del rol asignado','eval'],
                    ['Es capaz de admitir sus equivocaciones y recibir críticas','eval'],
                    ['Califique el aporte del compañero al éxito del proyecto','eval','clave'],
                    ['Fortalezas (indicar cuáles son)','txt'],
                    ['Debilidades (indicar cuáles son)','txt'],
                ]
        else:
            data  = []
            for pregunta in coevaluacion.pregunta_set.all():
                data.append([pregunta.title, pregunta.tipo])


        for row, row_data in enumerate(data, start=5): # start from row no.1
           for col, col_data in enumerate(row_data):
                 sheet.write(row, col, col_data, style=xlwt.Style.default_style)

        return book

class CoevaluacionImportExcelAlumno(ProfesorMixin, FormView):
    template_name = 'admin/coevaluaciones/coevaluaciones_import.html'
    form_class = XlsInputForm

    def get_context_data(self, **kwargs):
        context = super(CoevaluacionImportExcelAlumno,self).get_context_data(**kwargs)
        try:
            filter_query = Q(curso__profesor__pk = self.request.user.pk ) | Q(curso__ayudante__pk = self.request.user.pk )
            coevaluacion = Coevaluacion.objects.filter(filter_query).get(pk= self.kwargs.pop("coevaluacion_pk",None))
        except Coevaluacion.DoesNotExist:
            raise Http404

        context['coevaluacion'] = coevaluacion

        return context

    def form_valid(self, form):
        context = self.get_context_data(**self.kwargs)
        coevaluacion = context['coevaluacion']
        input_excel = form.cleaned_data['input_excel']

        coevaluacion_excel = xlrd.open_workbook(file_contents=input_excel.read())
        coevaluacion_hoja = coevaluacion_excel.sheet_by_index(0)
        #leo el documento
        #obtengo la primera Hoja
        # en la primera fila
        # for row in range(1, coevaluacion_hoja.nrows):
        preguntas = {}
        for pregunta in range(7, coevaluacion_hoja.ncols):
            pregunta_title = coevaluacion_hoja.cell_value(1, pregunta)
            preguntas[pregunta] = coevaluacion.pregunta_set.filter(title__icontains=pregunta_title)[0]

        for row in range(2, coevaluacion_hoja.nrows):
            coev_row = {
            'grupo': coevaluacion_hoja.cell_value(row, 4),
            'evaluador' : coevaluacion_hoja.cell_value(row, 5),
            'evaluado' : coevaluacion_hoja.cell_value(row, 6),
            }

            grupo = Grupo.objects.get(curso=coevaluacion.curso, nombre__iexact=coev_row['grupo'])
            estudiante_origen = grupo.get_alumno_by_nombre(coev_row['evaluador'])
            estudiante_evaluado = grupo.get_alumno_by_nombre(coev_row['evaluado'])
            for respuesta_index in range(7, coevaluacion_hoja.ncols):
                respuesta_kwargs = {
                    'grupo': grupo,
                    'pregunta': preguntas[respuesta_index],
                    'estudiante_evaluado': estudiante_evaluado,
                    'estudiante_origen': estudiante_origen,
                }
                try:
                    respuesta = Respuesta.objects.get(**respuesta_kwargs)
                except Respuesta.DoesNotExist:
                    respuesta = Respuesta(**respuesta_kwargs)
                value = coevaluacion_hoja.cell_value(row, respuesta_index)
                try:
                    if respuesta.pregunta.tipo == 'eval':
                        respuesta.respuesta_eval = value
                    else:
                        respuesta.respuesta_text = value

                    respuesta.save()

                except Exception, e:
                    messages.error(self.request, 'Error al subir Respuestas: %s'% str(e))
        messages.success(self.request, 'Respuestas subidas con éxito')


        return HttpResponseRedirect(reverse('admin_coevaluaciones:coevaluacion_resultados_profesor', args=[coevaluacion.pk]))



class CoevaluacionViewProfesor(ProfesorMixin, TemplateView):
    template_name = 'admin/coevaluaciones/coevaluaciones_list_view.html'
    
    def get_context_data(self, **kwargs):
        kwargs = super(CoevaluacionViewProfesor, self).get_context_data(**kwargs)
        try:
            coevaluacion = Coevaluacion.objects.filter(status='P').get(pk= kwargs.pop("coevaluacion_pk",None))
        except Coevaluacion.DoesNotExist:
            coevaluacion=None
        try:
            estudiante = Estudiante.objects.get(pk = self.request.user.pk)#get by session
            kwargs['estudiante'] = estudiante
        except Estudiante.DoesNotExist:
            estudiante=None
            kwargs['estudiante'] = self.request.user
        
        
        if coevaluacion and estudiante:
            Pregunta.objects.filter(coevaluacion=coevaluacion, respuesta__estudiante_evaluado__pk=estudiante.pk)
            grupo = coevaluacion.curso.get_grupo_by_alumno(estudiante)
            if grupo:
                if not coevaluacion.alumno_contesta(estudiante.pk):
                    kwargs['coevaluacion'] = coevaluacion
        return kwargs