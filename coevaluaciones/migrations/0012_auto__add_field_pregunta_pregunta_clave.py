# -*- coding: utf-8 -*-
import datetime
from south.db import db
from south.v2 import SchemaMigration
from django.db import models


class Migration(SchemaMigration):

    def forwards(self, orm):
        # Adding field 'Pregunta.pregunta_clave'
        db.add_column(u'coevaluaciones_pregunta', 'pregunta_clave',
                      self.gf('django.db.models.fields.BooleanField')(default=False),
                      keep_default=False)


    def backwards(self, orm):
        # Deleting field 'Pregunta.pregunta_clave'
        db.delete_column(u'coevaluaciones_pregunta', 'pregunta_clave')


    models = {
        u'auth.group': {
            'Meta': {'object_name': 'Group'},
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'unique': 'True', 'max_length': '80'}),
            'permissions': ('django.db.models.fields.related.ManyToManyField', [], {'to': u"orm['auth.Permission']", 'symmetrical': 'False', 'blank': 'True'})
        },
        u'auth.permission': {
            'Meta': {'ordering': "(u'content_type__app_label', u'content_type__model', u'codename')", 'unique_together': "((u'content_type', u'codename'),)", 'object_name': 'Permission'},
            'codename': ('django.db.models.fields.CharField', [], {'max_length': '100'}),
            'content_type': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['contenttypes.ContentType']"}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '50'})
        },
        u'auth.user': {
            'Meta': {'object_name': 'User'},
            'date_joined': ('django.db.models.fields.DateTimeField', [], {'default': 'datetime.datetime.now'}),
            'email': ('django.db.models.fields.EmailField', [], {'max_length': '75', 'blank': 'True'}),
            'first_name': ('django.db.models.fields.CharField', [], {'max_length': '30', 'blank': 'True'}),
            'groups': ('django.db.models.fields.related.ManyToManyField', [], {'to': u"orm['auth.Group']", 'symmetrical': 'False', 'blank': 'True'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'is_active': ('django.db.models.fields.BooleanField', [], {'default': 'True'}),
            'is_staff': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'is_superuser': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'last_login': ('django.db.models.fields.DateTimeField', [], {'default': 'datetime.datetime.now'}),
            'last_name': ('django.db.models.fields.CharField', [], {'max_length': '30', 'blank': 'True'}),
            'password': ('django.db.models.fields.CharField', [], {'max_length': '128'}),
            'user_permissions': ('django.db.models.fields.related.ManyToManyField', [], {'to': u"orm['auth.Permission']", 'symmetrical': 'False', 'blank': 'True'}),
            'username': ('django.db.models.fields.CharField', [], {'unique': 'True', 'max_length': '30'})
        },
        u'coevaluaciones.coevaluacion': {
            'Meta': {'ordering': "('position',)", 'unique_together': "(('curso', 'position'),)", 'object_name': 'Coevaluacion'},
            'curso': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['cursos.Curso']"}),
            'fecha_limite': ('django.db.models.fields.DateTimeField', [], {'null': 'True'}),
            'fecha_publicacion': ('django.db.models.fields.DateField', [], {'null': 'True'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'position': ('django.db.models.fields.PositiveSmallIntegerField', [], {}),
            'status': ('django.db.models.fields.CharField', [], {'default': "'C'", 'max_length': '1'})
        },
        u'coevaluaciones.fichacoevaluacion': {
            'Meta': {'object_name': 'FichaCoevaluacion'},
            'coevaluacion': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['coevaluaciones.Coevaluacion']"}),
            'grupo': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['cursos.Grupo']"}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'})
        },
        u'coevaluaciones.pregunta': {
            'Meta': {'ordering': "('position',)", 'object_name': 'Pregunta'},
            'coevaluacion': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['coevaluaciones.Coevaluacion']"}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'position': ('django.db.models.fields.PositiveSmallIntegerField', [], {}),
            'pregunta_clave': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'tipo': ('django.db.models.fields.CharField', [], {'default': "'eval'", 'max_length': '4'}),
            'title': ('django.db.models.fields.TextField', [], {})
        },
        u'coevaluaciones.respuesta': {
            'Meta': {'ordering': "('pregunta',)", 'unique_together': "(('pregunta', 'estudiante_origen', 'estudiante_evaluado', 'grupo'),)", 'object_name': 'Respuesta'},
            'estudiante_evaluado': ('django.db.models.fields.related.ForeignKey', [], {'related_name': "'estudiante_evaluado'", 'to': u"orm['cursos.Estudiante']"}),
            'estudiante_origen': ('django.db.models.fields.related.ForeignKey', [], {'related_name': "'estudiante_responde'", 'to': u"orm['cursos.Estudiante']"}),
            'grupo': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['cursos.Grupo']"}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'pregunta': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['coevaluaciones.Pregunta']"}),
            'respuesta': ('django.db.models.fields.CharField', [], {'max_length': '250'})
        },
        u'contenttypes.contenttype': {
            'Meta': {'ordering': "('name',)", 'unique_together': "(('app_label', 'model'),)", 'object_name': 'ContentType', 'db_table': "'django_content_type'"},
            'app_label': ('django.db.models.fields.CharField', [], {'max_length': '100'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'model': ('django.db.models.fields.CharField', [], {'max_length': '100'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '100'})
        },
        u'cursos.curso': {
            'Meta': {'object_name': 'Curso'},
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'nombre': ('django.db.models.fields.CharField', [], {'max_length': '100'}),
            'profesor': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['cursos.Profesor']"}),
            'semestre': ('django.db.models.fields.CharField', [], {'max_length': '100'})
        },
        u'cursos.estudiante': {
            'Meta': {'object_name': 'Estudiante', '_ormbases': [u'auth.User']},
            'curso': ('django.db.models.fields.related.ManyToManyField', [], {'symmetrical': 'False', 'to': u"orm['cursos.Curso']", 'null': 'True', 'blank': 'True'}),
            u'user_ptr': ('django.db.models.fields.related.OneToOneField', [], {'to': u"orm['auth.User']", 'unique': 'True', 'primary_key': 'True'})
        },
        u'cursos.grupo': {
            'Meta': {'unique_together': "(('curso', 'identificador'),)", 'object_name': 'Grupo'},
            'alumnos': ('django.db.models.fields.related.ManyToManyField', [], {'symmetrical': 'False', 'to': u"orm['cursos.Estudiante']", 'null': 'True', 'blank': 'True'}),
            'curso': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['cursos.Curso']"}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'identificador': ('django.db.models.fields.PositiveSmallIntegerField', [], {}),
            'nombre': ('django.db.models.fields.CharField', [], {'max_length': '50'})
        },
        u'cursos.profesor': {
            'Meta': {'object_name': 'Profesor', '_ormbases': [u'auth.User']},
            u'user_ptr': ('django.db.models.fields.related.OneToOneField', [], {'to': u"orm['auth.User']", 'unique': 'True', 'primary_key': 'True'})
        }
    }

    complete_apps = ['coevaluaciones']