# -*- coding: utf-8 -*-
import datetime
from south.db import db
from south.v2 import SchemaMigration
from django.db import models


class Migration(SchemaMigration):

    def forwards(self, orm):
        # Adding model 'Coevaluacion'
        db.create_table(u'coevaluaciones_coevaluacion', (
            (u'id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('curso', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['cursos.Curso'])),
            ('fecha', self.gf('django.db.models.fields.DateField')()),
            ('position', self.gf('django.db.models.fields.PositiveSmallIntegerField')()),
        ))
        db.send_create_signal(u'coevaluaciones', ['Coevaluacion'])

        # Adding unique constraint on 'Coevaluacion', fields ['curso', 'position']
        db.create_unique(u'coevaluaciones_coevaluacion', ['curso_id', 'position'])

        # Adding model 'FichaCoevaluacion'
        db.create_table(u'coevaluaciones_fichacoevaluacion', (
            (u'id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('coevaluacion', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['coevaluaciones.Coevaluacion'])),
            ('estudiante', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['cursos.Estudiante'])),
            ('grupo', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['cursos.Grupo'])),
        ))
        db.send_create_signal(u'coevaluaciones', ['FichaCoevaluacion'])

        # Adding unique constraint on 'FichaCoevaluacion', fields ['coevaluacion', 'estudiante']
        db.create_unique(u'coevaluaciones_fichacoevaluacion', ['coevaluacion_id', 'estudiante_id'])

        # Adding model 'Pregunta'
        db.create_table(u'coevaluaciones_pregunta', (
            (u'id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('coevaluacion', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['coevaluaciones.Coevaluacion'])),
            ('title', self.gf('django.db.models.fields.TextField')()),
        ))
        db.send_create_signal(u'coevaluaciones', ['Pregunta'])

        # Adding model 'Respuesta'
        db.create_table(u'coevaluaciones_respuesta', (
            (u'id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('pregunta', self.gf('django.db.models.fields.related.OneToOneField')(to=orm['coevaluaciones.Pregunta'], unique=True)),
            ('estudiante', self.gf('django.db.models.fields.related.ForeignKey')(related_name='estudiante_responde', to=orm['cursos.Estudiante'])),
            ('FichaCoevaluacion', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['coevaluaciones.FichaCoevaluacion'])),
            ('estudiante_critica', self.gf('django.db.models.fields.related.ForeignKey')(related_name='estudiante_evaluado', to=orm['cursos.Estudiante'])),
            ('respuesta', self.gf('django.db.models.fields.CharField')(max_length=20)),
        ))
        db.send_create_signal(u'coevaluaciones', ['Respuesta'])


    def backwards(self, orm):
        # Removing unique constraint on 'FichaCoevaluacion', fields ['coevaluacion', 'estudiante']
        db.delete_unique(u'coevaluaciones_fichacoevaluacion', ['coevaluacion_id', 'estudiante_id'])

        # Removing unique constraint on 'Coevaluacion', fields ['curso', 'position']
        db.delete_unique(u'coevaluaciones_coevaluacion', ['curso_id', 'position'])

        # Deleting model 'Coevaluacion'
        db.delete_table(u'coevaluaciones_coevaluacion')

        # Deleting model 'FichaCoevaluacion'
        db.delete_table(u'coevaluaciones_fichacoevaluacion')

        # Deleting model 'Pregunta'
        db.delete_table(u'coevaluaciones_pregunta')

        # Deleting model 'Respuesta'
        db.delete_table(u'coevaluaciones_respuesta')


    models = {
        u'auth.group': {
            'Meta': {'object_name': 'Group'},
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'unique': 'True', 'max_length': '80'}),
            'permissions': ('django.db.models.fields.related.ManyToManyField', [], {'to': u"orm['auth.Permission']", 'symmetrical': 'False', 'blank': 'True'})
        },
        u'auth.permission': {
            'Meta': {'ordering': "(u'content_type__app_label', u'content_type__model', u'codename')", 'unique_together': "((u'content_type', u'codename'),)", 'object_name': 'Permission'},
            'codename': ('django.db.models.fields.CharField', [], {'max_length': '100'}),
            'content_type': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['contenttypes.ContentType']"}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '50'})
        },
        u'auth.user': {
            'Meta': {'object_name': 'User'},
            'date_joined': ('django.db.models.fields.DateTimeField', [], {'default': 'datetime.datetime.now'}),
            'email': ('django.db.models.fields.EmailField', [], {'max_length': '75', 'blank': 'True'}),
            'first_name': ('django.db.models.fields.CharField', [], {'max_length': '30', 'blank': 'True'}),
            'groups': ('django.db.models.fields.related.ManyToManyField', [], {'to': u"orm['auth.Group']", 'symmetrical': 'False', 'blank': 'True'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'is_active': ('django.db.models.fields.BooleanField', [], {'default': 'True'}),
            'is_staff': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'is_superuser': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'last_login': ('django.db.models.fields.DateTimeField', [], {'default': 'datetime.datetime.now'}),
            'last_name': ('django.db.models.fields.CharField', [], {'max_length': '30', 'blank': 'True'}),
            'password': ('django.db.models.fields.CharField', [], {'max_length': '128'}),
            'user_permissions': ('django.db.models.fields.related.ManyToManyField', [], {'to': u"orm['auth.Permission']", 'symmetrical': 'False', 'blank': 'True'}),
            'username': ('django.db.models.fields.CharField', [], {'unique': 'True', 'max_length': '30'})
        },
        u'coevaluaciones.coevaluacion': {
            'Meta': {'ordering': "('position',)", 'unique_together': "(('curso', 'position'),)", 'object_name': 'Coevaluacion'},
            'curso': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['cursos.Curso']"}),
            'fecha': ('django.db.models.fields.DateField', [], {}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'position': ('django.db.models.fields.PositiveSmallIntegerField', [], {})
        },
        u'coevaluaciones.fichacoevaluacion': {
            'Meta': {'unique_together': "(('coevaluacion', 'estudiante'),)", 'object_name': 'FichaCoevaluacion'},
            'coevaluacion': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['coevaluaciones.Coevaluacion']"}),
            'estudiante': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['cursos.Estudiante']"}),
            'grupo': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['cursos.Grupo']"}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'})
        },
        u'coevaluaciones.pregunta': {
            'Meta': {'object_name': 'Pregunta'},
            'coevaluacion': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['coevaluaciones.Coevaluacion']"}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'title': ('django.db.models.fields.TextField', [], {})
        },
        u'coevaluaciones.respuesta': {
            'FichaCoevaluacion': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['coevaluaciones.FichaCoevaluacion']"}),
            'Meta': {'object_name': 'Respuesta'},
            'estudiante': ('django.db.models.fields.related.ForeignKey', [], {'related_name': "'estudiante_responde'", 'to': u"orm['cursos.Estudiante']"}),
            'estudiante_critica': ('django.db.models.fields.related.ForeignKey', [], {'related_name': "'estudiante_evaluado'", 'to': u"orm['cursos.Estudiante']"}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'pregunta': ('django.db.models.fields.related.OneToOneField', [], {'to': u"orm['coevaluaciones.Pregunta']", 'unique': 'True'}),
            'respuesta': ('django.db.models.fields.CharField', [], {'max_length': '20'})
        },
        u'contenttypes.contenttype': {
            'Meta': {'ordering': "('name',)", 'unique_together': "(('app_label', 'model'),)", 'object_name': 'ContentType', 'db_table': "'django_content_type'"},
            'app_label': ('django.db.models.fields.CharField', [], {'max_length': '100'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'model': ('django.db.models.fields.CharField', [], {'max_length': '100'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '100'})
        },
        u'cursos.curso': {
            'Meta': {'object_name': 'Curso'},
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'nombre': ('django.db.models.fields.CharField', [], {'max_length': '100'}),
            'profesor': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['cursos.Profesor']"}),
            'semestre': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['cursos.Semestre']"})
        },
        u'cursos.estudiante': {
            'Meta': {'object_name': 'Estudiante', '_ormbases': [u'auth.User']},
            u'user_ptr': ('django.db.models.fields.related.OneToOneField', [], {'to': u"orm['auth.User']", 'unique': 'True', 'primary_key': 'True'})
        },
        u'cursos.grupo': {
            'Meta': {'unique_together': "(('curso', 'identificador'),)", 'object_name': 'Grupo'},
            'alumnos': ('django.db.models.fields.related.ManyToManyField', [], {'to': u"orm['cursos.Estudiante']", 'symmetrical': 'False'}),
            'curso': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['cursos.Curso']"}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'identificador': ('django.db.models.fields.PositiveSmallIntegerField', [], {})
        },
        u'cursos.profesor': {
            'Meta': {'object_name': 'Profesor', '_ormbases': [u'auth.User']},
            u'user_ptr': ('django.db.models.fields.related.OneToOneField', [], {'to': u"orm['auth.User']", 'unique': 'True', 'primary_key': 'True'})
        },
        u'cursos.semestre': {
            'Meta': {'object_name': 'Semestre'},
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '100'}),
            'year': ('django.db.models.fields.DateField', [], {})
        }
    }

    complete_apps = ['coevaluaciones']