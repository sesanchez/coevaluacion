# -*- coding: utf-8 -*-
import datetime
from south.db import db
from south.v2 import SchemaMigration
from django.db import models


class Migration(SchemaMigration):

    def forwards(self, orm):
        # Removing unique constraint on 'Respuesta', fields ['pregunta', 'estudiante', 'estudiante_critica', 'FichaCoevaluacion']
        db.delete_unique(u'coevaluaciones_respuesta', ['pregunta_id', 'estudiante_id', 'estudiante_critica_id', 'FichaCoevaluacion_id'])

        # Deleting field 'Respuesta.FichaCoevaluacion'
        db.delete_column(u'coevaluaciones_respuesta', 'FichaCoevaluacion_id')

        # Deleting field 'Respuesta.estudiante'
        db.delete_column(u'coevaluaciones_respuesta', 'estudiante_id')

        # Deleting field 'Respuesta.estudiante_critica'
        db.delete_column(u'coevaluaciones_respuesta', 'estudiante_critica_id')

        # Adding field 'Respuesta.estudiante_origen'
        db.add_column(u'coevaluaciones_respuesta', 'estudiante_origen',
                      self.gf('django.db.models.fields.related.ForeignKey')(default=1, related_name='estudiante_responde', to=orm['cursos.Estudiante']),
                      keep_default=False)

        # Adding field 'Respuesta.grupo'
        db.add_column(u'coevaluaciones_respuesta', 'grupo',
                      self.gf('django.db.models.fields.related.ForeignKey')(default=1, to=orm['cursos.Grupo']),
                      keep_default=False)

        # Adding field 'Respuesta.estudiante_evaluado'
        db.add_column(u'coevaluaciones_respuesta', 'estudiante_evaluado',
                      self.gf('django.db.models.fields.related.ForeignKey')(default=1, related_name='estudiante_evaluado', to=orm['cursos.Estudiante']),
                      keep_default=False)


        # Changing field 'Respuesta.respuesta'
        db.alter_column(u'coevaluaciones_respuesta', 'respuesta', self.gf('django.db.models.fields.CharField')(max_length=250))
        # Adding unique constraint on 'Respuesta', fields ['pregunta', 'estudiante_origen', 'estudiante_evaluado', 'grupo']
        db.create_unique(u'coevaluaciones_respuesta', ['pregunta_id', 'estudiante_origen_id', 'estudiante_evaluado_id', 'grupo_id'])


    def backwards(self, orm):
        # Removing unique constraint on 'Respuesta', fields ['pregunta', 'estudiante_origen', 'estudiante_evaluado', 'grupo']
        db.delete_unique(u'coevaluaciones_respuesta', ['pregunta_id', 'estudiante_origen_id', 'estudiante_evaluado_id', 'grupo_id'])


        # User chose to not deal with backwards NULL issues for 'Respuesta.FichaCoevaluacion'
        raise RuntimeError("Cannot reverse this migration. 'Respuesta.FichaCoevaluacion' and its values cannot be restored.")
        
        # The following code is provided here to aid in writing a correct migration        # Adding field 'Respuesta.FichaCoevaluacion'
        db.add_column(u'coevaluaciones_respuesta', 'FichaCoevaluacion',
                      self.gf('django.db.models.fields.related.ForeignKey')(to=orm['coevaluaciones.FichaCoevaluacion']),
                      keep_default=False)


        # User chose to not deal with backwards NULL issues for 'Respuesta.estudiante'
        raise RuntimeError("Cannot reverse this migration. 'Respuesta.estudiante' and its values cannot be restored.")
        
        # The following code is provided here to aid in writing a correct migration        # Adding field 'Respuesta.estudiante'
        db.add_column(u'coevaluaciones_respuesta', 'estudiante',
                      self.gf('django.db.models.fields.related.ForeignKey')(related_name='estudiante_responde', to=orm['cursos.Estudiante']),
                      keep_default=False)


        # User chose to not deal with backwards NULL issues for 'Respuesta.estudiante_critica'
        raise RuntimeError("Cannot reverse this migration. 'Respuesta.estudiante_critica' and its values cannot be restored.")
        
        # The following code is provided here to aid in writing a correct migration        # Adding field 'Respuesta.estudiante_critica'
        db.add_column(u'coevaluaciones_respuesta', 'estudiante_critica',
                      self.gf('django.db.models.fields.related.ForeignKey')(related_name='estudiante_evaluado', to=orm['cursos.Estudiante']),
                      keep_default=False)

        # Deleting field 'Respuesta.estudiante_origen'
        db.delete_column(u'coevaluaciones_respuesta', 'estudiante_origen_id')

        # Deleting field 'Respuesta.grupo'
        db.delete_column(u'coevaluaciones_respuesta', 'grupo_id')

        # Deleting field 'Respuesta.estudiante_evaluado'
        db.delete_column(u'coevaluaciones_respuesta', 'estudiante_evaluado_id')


        # Changing field 'Respuesta.respuesta'
        db.alter_column(u'coevaluaciones_respuesta', 'respuesta', self.gf('django.db.models.fields.CharField')(max_length=20))
        # Adding unique constraint on 'Respuesta', fields ['pregunta', 'estudiante', 'estudiante_critica', 'FichaCoevaluacion']
        db.create_unique(u'coevaluaciones_respuesta', ['pregunta_id', 'estudiante_id', 'estudiante_critica_id', 'FichaCoevaluacion_id'])


    models = {
        u'auth.group': {
            'Meta': {'object_name': 'Group'},
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'unique': 'True', 'max_length': '80'}),
            'permissions': ('django.db.models.fields.related.ManyToManyField', [], {'to': u"orm['auth.Permission']", 'symmetrical': 'False', 'blank': 'True'})
        },
        u'auth.permission': {
            'Meta': {'ordering': "(u'content_type__app_label', u'content_type__model', u'codename')", 'unique_together': "((u'content_type', u'codename'),)", 'object_name': 'Permission'},
            'codename': ('django.db.models.fields.CharField', [], {'max_length': '100'}),
            'content_type': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['contenttypes.ContentType']"}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '50'})
        },
        u'auth.user': {
            'Meta': {'object_name': 'User'},
            'date_joined': ('django.db.models.fields.DateTimeField', [], {'default': 'datetime.datetime.now'}),
            'email': ('django.db.models.fields.EmailField', [], {'max_length': '75', 'blank': 'True'}),
            'first_name': ('django.db.models.fields.CharField', [], {'max_length': '30', 'blank': 'True'}),
            'groups': ('django.db.models.fields.related.ManyToManyField', [], {'to': u"orm['auth.Group']", 'symmetrical': 'False', 'blank': 'True'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'is_active': ('django.db.models.fields.BooleanField', [], {'default': 'True'}),
            'is_staff': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'is_superuser': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'last_login': ('django.db.models.fields.DateTimeField', [], {'default': 'datetime.datetime.now'}),
            'last_name': ('django.db.models.fields.CharField', [], {'max_length': '30', 'blank': 'True'}),
            'password': ('django.db.models.fields.CharField', [], {'max_length': '128'}),
            'user_permissions': ('django.db.models.fields.related.ManyToManyField', [], {'to': u"orm['auth.Permission']", 'symmetrical': 'False', 'blank': 'True'}),
            'username': ('django.db.models.fields.CharField', [], {'unique': 'True', 'max_length': '30'})
        },
        u'coevaluaciones.coevaluacion': {
            'Meta': {'ordering': "('position',)", 'unique_together': "(('curso', 'position'),)", 'object_name': 'Coevaluacion'},
            'curso': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['cursos.Curso']"}),
            'fecha': ('django.db.models.fields.DateField', [], {}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'position': ('django.db.models.fields.PositiveSmallIntegerField', [], {}),
            'status': ('django.db.models.fields.CharField', [], {'default': "'C'", 'max_length': '1'})
        },
        u'coevaluaciones.fichacoevaluacion': {
            'Meta': {'object_name': 'FichaCoevaluacion'},
            'coevaluacion': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['coevaluaciones.Coevaluacion']"}),
            'grupo': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['cursos.Grupo']"}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'})
        },
        u'coevaluaciones.pregunta': {
            'Meta': {'ordering': "('position',)", 'object_name': 'Pregunta'},
            'coevaluacion': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['coevaluaciones.Coevaluacion']"}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'position': ('django.db.models.fields.PositiveSmallIntegerField', [], {}),
            'tipo': ('django.db.models.fields.CharField', [], {'default': "'eval'", 'max_length': '4'}),
            'title': ('django.db.models.fields.TextField', [], {})
        },
        u'coevaluaciones.respuesta': {
            'Meta': {'unique_together': "(('pregunta', 'estudiante_origen', 'estudiante_evaluado', 'grupo'),)", 'object_name': 'Respuesta'},
            'estudiante_evaluado': ('django.db.models.fields.related.ForeignKey', [], {'related_name': "'estudiante_evaluado'", 'to': u"orm['cursos.Estudiante']"}),
            'estudiante_origen': ('django.db.models.fields.related.ForeignKey', [], {'related_name': "'estudiante_responde'", 'to': u"orm['cursos.Estudiante']"}),
            'grupo': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['cursos.Grupo']"}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'pregunta': ('django.db.models.fields.related.OneToOneField', [], {'to': u"orm['coevaluaciones.Pregunta']", 'unique': 'True'}),
            'respuesta': ('django.db.models.fields.CharField', [], {'max_length': '250'})
        },
        u'contenttypes.contenttype': {
            'Meta': {'ordering': "('name',)", 'unique_together': "(('app_label', 'model'),)", 'object_name': 'ContentType', 'db_table': "'django_content_type'"},
            'app_label': ('django.db.models.fields.CharField', [], {'max_length': '100'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'model': ('django.db.models.fields.CharField', [], {'max_length': '100'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '100'})
        },
        u'cursos.curso': {
            'Meta': {'object_name': 'Curso'},
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'nombre': ('django.db.models.fields.CharField', [], {'max_length': '100'}),
            'profesor': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['cursos.Profesor']"}),
            'semestre': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['cursos.Semestre']"})
        },
        u'cursos.estudiante': {
            'Meta': {'object_name': 'Estudiante', '_ormbases': [u'auth.User']},
            u'user_ptr': ('django.db.models.fields.related.OneToOneField', [], {'to': u"orm['auth.User']", 'unique': 'True', 'primary_key': 'True'})
        },
        u'cursos.grupo': {
            'Meta': {'unique_together': "(('curso', 'identificador'),)", 'object_name': 'Grupo'},
            'alumnos': ('django.db.models.fields.related.ManyToManyField', [], {'to': u"orm['cursos.Estudiante']", 'symmetrical': 'False'}),
            'curso': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['cursos.Curso']"}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'identificador': ('django.db.models.fields.PositiveSmallIntegerField', [], {})
        },
        u'cursos.profesor': {
            'Meta': {'object_name': 'Profesor', '_ormbases': [u'auth.User']},
            u'user_ptr': ('django.db.models.fields.related.OneToOneField', [], {'to': u"orm['auth.User']", 'unique': 'True', 'primary_key': 'True'})
        },
        u'cursos.semestre': {
            'Meta': {'object_name': 'Semestre'},
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '100'}),
            'year': ('django.db.models.fields.DateField', [], {})
        }
    }

    complete_apps = ['coevaluaciones']