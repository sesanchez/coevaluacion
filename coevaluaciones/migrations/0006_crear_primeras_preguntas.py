# -*- coding: utf-8 -*-
import datetime
from south.db import db
from south.v2 import DataMigration
from django.db import models

class Migration(DataMigration):

    def forwards(self, orm):
        pass
#         coevaluacion = orm.Coevaluacion.objects.all()[0]
# 
#         preguntas = orm.Pregunta.objects.all()
#         preguntas.delete()
#         pregunta1 = orm.Pregunta(coevaluacion=coevaluacion, title=u"Demuestra compromiso con el proyecto ", position=1, tipo="eval", )
#         pregunta1.save()
#         pregunta2 = orm.Pregunta(coevaluacion=coevaluacion, title=u"Cumple de manera adecuada con las tareas que le son asignadas ", position=2, tipo="eval", )
#         pregunta2.save()
#         pregunta3 = orm.Pregunta(coevaluacion=coevaluacion, title=u"Demuestra iniciativa para lograr el éxito del proyecto", position=3, tipo="eval", )
#         pregunta3.save()
#         pregunta4 = orm.Pregunta(coevaluacion=coevaluacion, title=u"Mantiene buena comunicación con el resto del equipo", position=4, tipo="eval", )
#         pregunta4.save()
#         pregunta5 = orm.Pregunta(coevaluacion=coevaluacion, title=u"Mantiene buena coordinación entre sus tareas y las de sus pares", position=5, tipo="eval", )
#         pregunta5.save()
#         pregunta6 = orm.Pregunta(coevaluacion=coevaluacion, title=u"La calidad de su trabajo es la apropiada para lograr el éxito del proyecto.", position=6, tipo="eval", )
#         pregunta6.save()
#         pregunta7 = orm.Pregunta(coevaluacion=coevaluacion, title=u"Ofrece apoyo en tareas que van más allá del rol asignado", position=7, tipo="eval", )
#         pregunta7.save()
#         pregunta8 = orm.Pregunta(coevaluacion=coevaluacion, title=u"Es capaz de admitir sus equivocaciones y recibir críticas", position=8, tipo="eval", )
#         pregunta8.save()
#         pregunta9 = orm.Pregunta(coevaluacion=coevaluacion, title=u"Fortalezas (indicar cuáles son)", position=9, tipo="txt", )
#         pregunta9.save()
#         pregunta10 = orm.Pregunta(coevaluacion=coevaluacion, title=u"Debilidades (indicar cuáles son)", position=10, tipo="txt", )
#         pregunta10.save()
# Note: Don't use "from appname.models import ModelName". 
# Use orm.ModelName to refer to models in this application,
# and orm['appname.ModelName'] for models in other applications.
    def backwards(self, orm):
        pass
        "Write your backwards methods here."

    models = {
        u'auth.group': {
            'Meta': {'object_name': 'Group'},
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'unique': 'True', 'max_length': '80'}),
            'permissions': ('django.db.models.fields.related.ManyToManyField', [], {'to': u"orm['auth.Permission']", 'symmetrical': 'False', 'blank': 'True'})
        },
        u'auth.permission': {
            'Meta': {'ordering': "(u'content_type__app_label', u'content_type__model', u'codename')", 'unique_together': "((u'content_type', u'codename'),)", 'object_name': 'Permission'},
            'codename': ('django.db.models.fields.CharField', [], {'max_length': '100'}),
            'content_type': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['contenttypes.ContentType']"}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '50'})
        },
        u'auth.user': {
            'Meta': {'object_name': 'User'},
            'date_joined': ('django.db.models.fields.DateTimeField', [], {'default': 'datetime.datetime.now'}),
            'email': ('django.db.models.fields.EmailField', [], {'max_length': '75', 'blank': 'True'}),
            'first_name': ('django.db.models.fields.CharField', [], {'max_length': '30', 'blank': 'True'}),
            'groups': ('django.db.models.fields.related.ManyToManyField', [], {'to': u"orm['auth.Group']", 'symmetrical': 'False', 'blank': 'True'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'is_active': ('django.db.models.fields.BooleanField', [], {'default': 'True'}),
            'is_staff': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'is_superuser': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'last_login': ('django.db.models.fields.DateTimeField', [], {'default': 'datetime.datetime.now'}),
            'last_name': ('django.db.models.fields.CharField', [], {'max_length': '30', 'blank': 'True'}),
            'password': ('django.db.models.fields.CharField', [], {'max_length': '128'}),
            'user_permissions': ('django.db.models.fields.related.ManyToManyField', [], {'to': u"orm['auth.Permission']", 'symmetrical': 'False', 'blank': 'True'}),
            'username': ('django.db.models.fields.CharField', [], {'unique': 'True', 'max_length': '30'})
        },
        u'coevaluaciones.coevaluacion': {
            'Meta': {'ordering': "('position',)", 'unique_together': "(('curso', 'position'),)", 'object_name': 'Coevaluacion'},
            'curso': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['cursos.Curso']"}),
            'fecha': ('django.db.models.fields.DateField', [], {}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'position': ('django.db.models.fields.PositiveSmallIntegerField', [], {}),
            'status': ('django.db.models.fields.CharField', [], {'default': "'C'", 'max_length': '1'})
        },
        u'coevaluaciones.fichacoevaluacion': {
            'Meta': {'object_name': 'FichaCoevaluacion'},
            'coevaluacion': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['coevaluaciones.Coevaluacion']"}),
            'grupo': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['cursos.Grupo']"}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'})
        },
        u'coevaluaciones.pregunta': {
            'Meta': {'ordering': "('position',)", 'object_name': 'Pregunta'},
            'coevaluacion': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['coevaluaciones.Coevaluacion']"}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'position': ('django.db.models.fields.PositiveSmallIntegerField', [], {}),
            'tipo': ('django.db.models.fields.CharField', [], {'default': "'eval'", 'max_length': '4'}),
            'title': ('django.db.models.fields.TextField', [], {})
        },
        u'coevaluaciones.respuesta': {
            'FichaCoevaluacion': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['coevaluaciones.FichaCoevaluacion']"}),
            'Meta': {'unique_together': "(('pregunta', 'estudiante', 'estudiante_critica', 'FichaCoevaluacion'),)", 'object_name': 'Respuesta'},
            'estudiante': ('django.db.models.fields.related.ForeignKey', [], {'related_name': "'estudiante_responde'", 'to': u"orm['cursos.Estudiante']"}),
            'estudiante_critica': ('django.db.models.fields.related.ForeignKey', [], {'related_name': "'estudiante_evaluado'", 'to': u"orm['cursos.Estudiante']"}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'pregunta': ('django.db.models.fields.related.OneToOneField', [], {'to': u"orm['coevaluaciones.Pregunta']", 'unique': 'True'}),
            'respuesta': ('django.db.models.fields.CharField', [], {'max_length': '20'})
        },
        u'contenttypes.contenttype': {
            'Meta': {'ordering': "('name',)", 'unique_together': "(('app_label', 'model'),)", 'object_name': 'ContentType', 'db_table': "'django_content_type'"},
            'app_label': ('django.db.models.fields.CharField', [], {'max_length': '100'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'model': ('django.db.models.fields.CharField', [], {'max_length': '100'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '100'})
        },
        u'cursos.curso': {
            'Meta': {'object_name': 'Curso'},
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'nombre': ('django.db.models.fields.CharField', [], {'max_length': '100'}),
            'profesor': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['cursos.Profesor']"}),
            'semestre': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['cursos.Semestre']"})
        },
        u'cursos.estudiante': {
            'Meta': {'object_name': 'Estudiante', '_ormbases': [u'auth.User']},
            u'user_ptr': ('django.db.models.fields.related.OneToOneField', [], {'to': u"orm['auth.User']", 'unique': 'True', 'primary_key': 'True'})
        },
        u'cursos.grupo': {
            'Meta': {'unique_together': "(('curso', 'identificador'),)", 'object_name': 'Grupo'},
            'alumnos': ('django.db.models.fields.related.ManyToManyField', [], {'to': u"orm['cursos.Estudiante']", 'symmetrical': 'False'}),
            'curso': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['cursos.Curso']"}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'identificador': ('django.db.models.fields.PositiveSmallIntegerField', [], {})
        },
        u'cursos.profesor': {
            'Meta': {'object_name': 'Profesor', '_ormbases': [u'auth.User']},
            u'user_ptr': ('django.db.models.fields.related.OneToOneField', [], {'to': u"orm['auth.User']", 'unique': 'True', 'primary_key': 'True'})
        },
        u'cursos.semestre': {
            'Meta': {'object_name': 'Semestre'},
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '100'}),
            'year': ('django.db.models.fields.DateField', [], {})
        }
    }

    complete_apps = ['coevaluaciones']
    symmetrical = True
