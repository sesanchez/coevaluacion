from django.conf import settings
from django import template
from coevaluaciones.models import Respuesta

register = template.Library()

### Filters

@register.filter
def contestada(coevaluacion, estuadiante_pk):
    respondio = coevaluacion.alumno_contestada(estuadiante_pk)
    return respondio
    
@register.filter
def get_promedio_pregunta_clave(estudiante, coevaluacion_pk):
    respuestas = Respuesta.objects.filter(estudiante_evaluado = estudiante, pregunta__tipo="eval", pregunta__pregunta_clave=True, pregunta__coevaluacion__pk = coevaluacion_pk)
    value = 0
    if respuestas:
        for respuesta in respuestas:
            value += respuesta.respuesta_eval
        return '{0:.2g}'.format(float(value) / respuestas.count())
    else:
        return "Sin respuesta"

@register.filter
def get_promedio_preguntas(estudiante, coevaluacion_pk):
    respuestas = Respuesta.objects.filter(estudiante_evaluado = estudiante, pregunta__tipo="eval", pregunta__pregunta_clave=False, pregunta__coevaluacion__pk = coevaluacion_pk)
    value = 0
    if respuestas:
        for respuesta in respuestas:
            value += respuesta.respuesta_eval
        return '{0:.2g}'.format(float(value) / respuestas.count())
    else:
        return "Sin respuestas"

@register.filter
def get_promedio_pregunta(estudiante, pregunta_pk):
    respuestas = Respuesta.objects.filter(estudiante_evaluado = estudiante, pregunta__tipo="eval", pregunta__pk=pregunta_pk)
    value = 0
    if respuestas:
        for respuesta in respuestas:
            value += respuesta.respuesta_eval
        return '{0:.2g}'.format(float(value) / respuestas.count())
    else:
        return "Sin respuestas"
        

