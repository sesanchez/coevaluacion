# -*- coding: utf-8 -*-
from datetime import datetime
from django.db import models
from django.db.models.base import Model
from django.db.models.fields import CharField, DateField, TextField,\
    PositiveSmallIntegerField, DateTimeField, BooleanField
from django.db.models.fields.related import ForeignKey, OneToOneField
from cursos.models import Curso, Estudiante, Grupo
from django.utils.encoding import smart_unicode
from django.core.validators import MaxLengthValidator

ESTADO_COEVALUACION = (
    ('C', 'Creada'),
    ('P', 'Publicada'),
    ('F', 'Finalizada'),
)  
    
TIPO_PREGUNTAS = (
    ('txt', 'opinion'),
    ('eval', 'evaluativa'),
)

BOOL_CHOICES = ((True, 'Sí'), (False, 'No'))

class Coevaluacion(Model):

    curso = ForeignKey(Curso)
    fecha_publicacion = DateField(null=True)
    fecha_limite = DateTimeField(null=True)
    status = CharField(max_length=1, default='C', choices = ESTADO_COEVALUACION)
    position = PositiveSmallIntegerField("Position")
    nombre = CharField(max_length=100, null=True, blank=True)
    
    updated = DateTimeField(auto_now=True)
    created = DateTimeField(auto_now_add=True)

    def get_nombre(self):
        if self.nombre:
            return self.nombre
        else:
            return u"coevaluación %s" % (self.position)

    def get_status(self):
        #devuelve status, si la coevaluacion ya ha finalizado, actualiza el estado
        if self.status == 'P' and self.fecha_limite and datetime.now() > self.fecha_limite:
            self.status = 'F'
            self.save()
        return self.status

    def validar_curso(self):
        #devuelve true si el curso de la coevaluacion es apto para responder una coevaluacion
        if self.curso.grupo_set.count() == 0:
            return False
        #debe tener al menos un grupo
        #y los grupos no pueden ser de un alumno
        return False not in [grupo.alumnos.count()!= 1 for grupo in self.curso.grupo_set.all()]

    def numero_alumnos_contestan(self):
        return self.curso.estudiante_set.filter(estudiante_responde__pregunta__coevaluacion=self, estudiante_responde__isnull=False).distinct().count()

    def numero_alumnos(self):
        return  self.curso.estudiante_set.distinct().count()


    def validar_preguntas(self):
        #devuelve true si la coevaluacion tiene al menos una pregunta
        return self.pregunta_set.count() > 0

    
    def get_dias_para_cierre(self):
        #devuelve dias para status, si la coevaluacion ya ha finalizado, actualiza el estado
        if self.status == 'P' and self.fecha_limite and datetime.now() < self.fecha_limite:
            return (self.fecha_limite - datetime.now()).days
        return -1

    def alumno_contestada(self, estudiante_pk):
        #devuelve True, si la coevaluacion tiene respuestas del estudiante de origen
        return 0 < len(self.pregunta_set.filter(respuesta__estudiante_origen__pk=estudiante_pk))

    def __unicode__(self):
        return unicode(self.__str__())

    def __str__(self): 
        return  self.get_nombre()+" "+self.curso.nombre

    class Meta:
        unique_together = ("curso", "position")
        ordering = ('-created', )
        verbose_name_plural = "Coevaluaciones"
        permissions = (
            ("alumno", "alumno"),
            ("profesor", "profesor"),
        )

        
class Pregunta(Model):
    coevaluacion = ForeignKey(Coevaluacion)
    title = TextField()
    tipo = CharField(max_length=4, default='eval', choices = TIPO_PREGUNTAS)
    position = PositiveSmallIntegerField("Position")
    pregunta_clave = BooleanField()

    def __unicode__(self):
        return unicode(self.__str__())
    
    def __str__(self):
        return "%d. %s" % (self.position +1, self.title)

    class Meta:
        verbose_name_plural = "Preguntas"
        ordering = ('position', )
        permissions = (
            ("alumno", "alumno"),
            ("profesor", "profesor"),
        )
    
class Respuesta(Model):
    pregunta = ForeignKey(Pregunta)
    estudiante_origen = ForeignKey(Estudiante, related_name="estudiante_responde")
    grupo  = ForeignKey(Grupo)
    estudiante_evaluado  = ForeignKey(Estudiante, related_name="estudiante_evaluado")
    respuesta_eval = PositiveSmallIntegerField(null=True, blank=True)
    respuesta_text = TextField(validators=[MaxLengthValidator(2000)], null=True, blank=True)
    
    def get_respuesta(self):
        if self.pregunta.tipo == 'eval':
            
            return self.respuesta_eval
        else:
            return self.respuesta_text
    
    def __unicode__(self):
        return unicode(self.__str__())
    
    def __str__(self):
        return "%s --> %s, pregunta %d" % (self.estudiante_origen, self.estudiante_evaluado, self.pregunta.position)

    class Meta:
        unique_together = ("pregunta","estudiante_origen","estudiante_evaluado","grupo")
        verbose_name_plural = "Respuestas"
        ordering = ('pregunta', )
        permissions = (
            ("alumno", "alumno"),
            ("profesor", "profesor"),
        )


    