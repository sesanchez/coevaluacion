from setting_base import *

DEBUG = False

DATABASES['default']['PASSWORD'] = 'B32XKQgfhKkMTKa'

ALLOWED_HOSTS = ['148.251.35.189']

STATIC_ROOT = '/var/www/coevaluaciones/website/static/'
STATIC_URL = '/static/'

TEMPLATE_DIRS = (
    os.path.join(os.path.dirname(__file__), 'templates'),
    os.path.join("/var/www/coevaluaciones/website/templates", 'templates'),
    
    # Put strings here, like "/home/html/django_templates" or "C:/www/django/templates".
    # Always use forward slashes, even on Windows.
    # Don't forget to use absolute paths, not relative paths.
)
