# coding=utf-8
from django.views.generic.edit import UpdateView

from .models import Estudiante
from django.core.urlresolvers import reverse
from cursos.forms import CambiarPasswordForm
from django.http.response import Http404, HttpResponseRedirect
from django.contrib.auth import login, authenticate  
from django.contrib.auth import authenticate
from django.contrib import messages

class PrimerAccesoEstudiante(UpdateView):
    model = Estudiante
    template_name = 'cursos/estudiante_first_access.html'
    form_class = CambiarPasswordForm
    not_user = False

    def get(self, request, *args, **kwargs):
        get_response  = super(PrimerAccesoEstudiante, self).get(request, *args, **kwargs)
        if self.not_user:
            return HttpResponseRedirect(reverse('login_view'))
        return get_response

    def post(self, request, *args, **kwargs):
        post_response  = super(PrimerAccesoEstudiante, self).post(request, *args, **kwargs)
        if self.not_user:
            return HttpResponseRedirect(reverse('login_view'))
        return post_response

    def get_context_data(self, **kwargs):
        kwargs = super(PrimerAccesoEstudiante, self).get_context_data(**kwargs)
        password = "pbkdf2_sha256$%s"%self.kwargs.pop("hash_password",None)
        try:
            estudiante = Estudiante.objects.get(primer_acceso=False, pk = self.object.pk, password__exact=password)
            kwargs['estudiante']=estudiante
            kwargs['usuario'] = estudiante
        except Estudiante.DoesNotExist:
            self.not_user = True
            messages.warning(self.request, u'Ops... el link de acceso ha caducado.')
        return kwargs
    
    def form_valid(self, form):
        estudiante = form.save(commit=False)
        password = estudiante.password
        estudiante.set_password(password)
        estudiante.primer_acceso = True
        estudiante.save()
        user = authenticate(username=estudiante.username, password=password)
        login(self.request, user) 
        messages.success(self.request, u'Bienvenido al sistema de coevaluaciones, tu nombre de usuario es <b>%s</b>'%estudiante.username)
        return HttpResponseRedirect(self.get_success_url()) 

    def get_success_url(self):
        return reverse('admin_coevaluaciones:ver_coevaluaciones_alumno')
