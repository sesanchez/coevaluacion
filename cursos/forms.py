# coding=utf-8
import re
from django.forms.models import ModelForm
from cursos.models import Estudiante
from django.forms.fields import CharField
from django.forms.widgets import PasswordInput
from django.core.exceptions import ValidationError


class CambiarPasswordForm(ModelForm):
    password = CharField(label="Contraseña",widget=PasswordInput())
    confirm_password = CharField(label="Confirmar contraseña", widget=PasswordInput())
       
    def clean(self):
        cleaned_data = super(CambiarPasswordForm, self).clean()
        password = cleaned_data.get("password")
        confirm_password = cleaned_data.get("confirm_password")
        if confirm_password != password:
            raise ValidationError("Contraseñas no coinciden.")
        if password and len(password) <= 5:
            raise ValidationError('Su contraseña debe tener al menos 6 caracteres.')
        if not re.match(r'[A-z0-9]+', password):
            raise ValidationError('Solo caracteres alfanuméricos permitidos.')
        else:
            return cleaned_data
    
    class Meta:
        model = Estudiante
        fields = ('password',)