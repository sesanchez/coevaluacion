# -*- coding: utf-8 -*-

from django.contrib.auth.models import User
from django.db.models import Q
from django.db.models.base import Model
from django.db.models.fields import CharField, BooleanField, DateField, PositiveSmallIntegerField
from django.db.models.fields.related import ForeignKey, ManyToManyField



class Profesor(User):
    class Meta:
        verbose_name_plural = "Profesores"
        permissions = (
            ("profesor", "profesor"),)

    def __unicode__(self):
        return unicode(self.__str__())

    def __str__(self):
        return self.first_name +' '+ self.last_name

class Ayudante(User):
    class Meta:
        verbose_name_plural = "Ayudantes"
        permissions = (
            ("ayudante", "ayudante"),)

    def __unicode__(self):
        return unicode(self.__str__())

    def __str__(self):
        return self.first_name +' '+ self.last_name  

class Curso(Model):
    profesor = ForeignKey(Profesor)
    ayudante = ForeignKey(Ayudante, null=True)
    nombre = CharField(max_length=100)
    semestre = CharField(max_length=100)

    def get_alumnos(self):
        alumnos_list = []
        for grupo in  self.grupo_set.all():
            alumnos_list += grupo.alumnos.all()
        return alumnos_list

    def get_alumnos_sin_grupo(self):
        return self.estudiante_set.exclude(grupo__in=self.grupo_set.all())


    def get_grupo_by_alumno(self, alumno):
        for grupo in  self.grupo_set.all():
            if alumno in grupo.alumnos.all():
                return grupo
        return None

    def __unicode__(self):
        return unicode(self.__str__())


    def __str__(self):
        return "%s, %s %s" % (self.nombre, self.profesor.first_name, self.profesor.last_name)
    


    class Meta:
        verbose_name_plural = "Cursos"
        permissions = (
            ("alumno", "alumno"),
            ("profesor", "profesor"),)
        
class Estudiante(User):
    curso = ManyToManyField(Curso, blank=True, null=True)
    primer_acceso = BooleanField()
    class Meta:
        verbose_name_plural = "Estudiantes"
        permissions = (
            ("alumno", "alumno"),)

    def get_grupo_curso(self, curso_pk):
        grupo = self.grupo_set.filter(curso__pk=curso_pk )
        if grupo:
            return grupo[0]
        return None

class Grupo(Model):
    curso = ForeignKey(Curso)
    identificador = PositiveSmallIntegerField("Position")
    nombre = CharField(max_length=50)
    alumnos = ManyToManyField(Estudiante, blank=True, null=True)

    def get_alumno_by_nombre(self, nombre):
        nombres = nombre.split(' ')
        query_first_name = Q()
        query_last_name = Q()

        for nombre in nombres:
            query_first_name |= Q(first_name__iexact=' '.join(nombres[0:nombres.index(nombre)+1]))
            query_last_name |= Q(last_name__iexact=' '.join(nombres[nombres.index(nombre):len(nombres)]))

        alumno = self.alumnos.filter(query_first_name, query_last_name)
        if alumno:
            return alumno[0]
        else:
            return None

    def __unicode__(self):
        return unicode(self.__str__())

    def __str__(self):
        return "%d.- %s" % (self.identificador, self.nombre)
    
    class Meta:
        unique_together = ("curso", "identificador")
        ordering = ('identificador',)
        verbose_name_plural = "Grupos"
        permissions = (
            ("alumno", "alumno"),
            ("profesor", "profesor"),)