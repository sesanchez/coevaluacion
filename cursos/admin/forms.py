# -*- coding: UTF-8 -*-
import os
import re
from django.forms.forms import Form
from django.forms import ValidationError, ModelForm
from django.forms.fields import FileField, CharField
from ..models import Grupo, Curso
from django.forms.widgets import PasswordInput
from cursos.models import Estudiante
from django.contrib.auth.models import User

IMPORT_FILE_TYPES = ['.xls','.xlsx' ]

class XlsInputForm(Form):
    input_excel = FileField(required= True, label= u"Importar Alumnps y grupos una hoja de calculos.")

    def clean_input_excel(self):
        input_excel = self.cleaned_data['input_excel']
        extension = os.path.splitext( input_excel.name )[1]
        if not (extension in IMPORT_FILE_TYPES):
            raise ValidationError( u'%s No es un archivo valido' % extension )
        else:
            return input_excel


class XlsInputAlumnosForm(XlsInputForm):
    input_excel = FileField(required= True, label= u"Importar Alumnos una hoja de calculos.")


class CrateGroupForm(ModelForm):

    class Meta:
        model = Grupo
        exclude = ('curso', 'alumnos')


class CrateCursoForm(ModelForm):
    def __init__(self, *args, **kwargs):
        super(CrateCursoForm, self).__init__(*args, **kwargs)
        
    class Meta:
        model = Curso
        exclude = ('profesor', 'ayudante')


class CambiarPasswordForm(ModelForm):
    old_password = CharField(label="Contraseña actual", widget=PasswordInput())
    password = CharField(label="Nueva contraseña", widget=PasswordInput())
    confirm_password = CharField(label="Verificar contraseña" ,widget=PasswordInput())
    
    def __init__(self, *args, **kwargs):
        self.usuario = kwargs.pop('usuario', None)
        super(CambiarPasswordForm, self).__init__(*args, **kwargs)
    
    def clean(self):
        cleaned_data = super(CambiarPasswordForm, self).clean()
        password = cleaned_data.get("password")
        confirm_password = cleaned_data.get("confirm_password")
        if confirm_password != password:
            raise ValidationError("Contraseñas no coinciden.")
        if password and len(password) <= 5:
            raise ValidationError('Su contraseña debe tener al menos 6 caracteres.')
        if not re.match(r'[A-z0-9]+', password):
            raise ValidationError('Solo caracteres alfanuméricos permitidos.')
        else:
            return cleaned_data
    
    def clean_old_password(self):
        """
        Validates that the old_password field is correct.
        """
        old_password = self.cleaned_data["old_password"]
        if not self.usuario.check_password(old_password):
            raise ValidationError('La contraseña que has ingresado es incorrecta. Por favor ingresa una contraseña diferente.')
        return old_password
    
    class Meta:
        model = User
        fields = ('password',)