# -*- coding: utf-8 -*-
from ..models import Curso, Grupo, Profesor, Estudiante
from django.contrib import admin
from django.contrib.admin.widgets import FilteredSelectMultiple
from django.conf.urls import patterns
from django.db.models import Q
from django.contrib.admin.sites import AlreadyRegistered


class GrupoInline(admin.StackedInline):
	filter_horizontal = ['alumnos']

	def formfield_for_manytomany(self, db_field, request, **kwargs):
		curso = self.get_parent_object(request,Curso)
		if curso and db_field.name == "alumnos":
			kwargs["queryset"] = Estudiante.objects.filter(curso__pk=curso.pk)
			#kwargs["widget"]   = FilteredSelectMultiple
		return super(GrupoInline, self).formfield_for_manytomany(db_field, request, **kwargs)


	def get_parent_object(self, request, model):
		object_id = request.META['PATH_INFO'].strip('/').split('/')[-1]
		try:
			object_id = int(object_id)
		except ValueError:
			return None
		return model.objects.get(pk=object_id)
	


	
	model = Grupo
	extra = 0

	

class CursoAdmin(admin.ModelAdmin):
	list_display = ['profesor', 'nombre', 'semestre']
	inlines = [GrupoInline]
	model = Curso

try:
	admin.site.register(Curso, CursoAdmin)
	admin.site.register(Estudiante)
	admin.site.register(Profesor)
except AlreadyRegistered:
	pass