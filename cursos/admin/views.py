# -*- coding: utf-8 -*-
import xlrd
import xlwt

from django.http import HttpResponse
from django.db import IntegrityError
from django.views.generic import View, TemplateView
from django.views.generic.base import RedirectView
from django.views.generic.detail import DetailView, SingleObjectMixin
from django.views.generic.edit import FormView, CreateView, UpdateView
from django.contrib import messages
from django.contrib.auth.models import Group, User
from django.shortcuts import redirect, get_object_or_404
from django.core.urlresolvers import reverse
from django.http import HttpResponseRedirect, HttpResponse
from django.http.response import Http404

from ..models import Grupo, Estudiante, Profesor, Curso
from .forms import XlsInputForm, CrateGroupForm, XlsInputAlumnosForm, CrateGroupForm
from cursos.admin.forms import CrateCursoForm, CambiarPasswordForm
from cursos.models import Ayudante
from django.db.models.query_utils import Q

class CursoAlumnoVerMisCursosView(DetailView):
    template_name = 'admin/cursos/alumno/ver_miscursos.html'
    model = Estudiante
    slug_field = 'pk'
    slug_url_kwarg = 'alumno_pk'
    context_object_name = 'alumno'

class CursoProfesorVerMisCursosView(DetailView):
    template_name = 'admin/cursos/profesor/ver_miscursos.html'
    model = Profesor
    slug_field = 'pk'
    slug_url_kwarg = 'profesor_pk'
    context_object_name = 'profesor'

class CursoAyudanteVerMisCursosView(DetailView):
    template_name = 'admin/cursos/profesor/ver_miscursos.html'
    model = Ayudante
    slug_field = 'pk'
    slug_url_kwarg = 'ayudante_pk'
    context_object_name = 'profesor'

class CursoDetailViewProfesor(DetailView):
    template_name = 'admin/cursos/curso_detail_view_profesor.html'
    model = Curso
    slug_field = 'pk'
    slug_url_kwarg = 'curso_pk'
    context_object_name = 'curso'

    def get_context_data(self, **kwargs):
        kwargs = super(CursoDetailViewProfesor, self).get_context_data(**kwargs)
        if self.object.profesor.pk != self.request.user.pk:
            raise Http404

        kwargs['usuario'] = self.request.user
        kwargs['rol'] = 'profesor'
        return kwargs

class CursoDetailViewAyudante(DetailView):
    template_name = 'admin/cursos/curso_detail_view_profesor.html'
    model = Curso
    slug_field = 'pk'
    slug_url_kwarg = 'curso_pk'
    context_object_name = 'curso'

    def get_context_data(self, **kwargs):
        if self.object.ayudante.pk != self.request.user.pk:
            raise Http404
        kwargs = super(CursoDetailViewProfesor, self).get_context_data(**kwargs)
        kwargs['usuario'] = self.request.user
        kwargs['rol'] = 'ayudante'
        return kwargs

class CrearCursoProfesorView(CreateView):
    model = Curso
    template_name = 'admin/cursos/create_curso_profesor.html'
    form_class = CrateCursoForm

    def get_context_data(self, **kwargs):
        kwargs = super(CrearCursoProfesorView, self).get_context_data(**kwargs)
        cursos = Curso.objects.filter(profesor__pk = self.request.user.pk)
        kwargs['cursos']= cursos
        kwargs['usuario'] = self.request.user
        kwargs['rol'] = 'profesor'
        return kwargs

    def form_valid(self,form):
        curso = form.save(commit=False)
        curso.profesor = get_object_or_404(Profesor,pk=self.request.user.pk)
        curso.save()
        return HttpResponseRedirect(self.get_success_url())


    def get_success_url(self):
        return reverse('admin_cursos:ver_cursos_profesor')
    
class EditCursoProfesor(UpdateView):
    model = Curso
    template_name = 'admin/cursos/create_curso_profesor.html'
    form_class = CrateCursoForm

    def get_context_data(self, **kwargs):
        kwargs = super(EditCursoProfesor, self).get_context_data(**kwargs)
        cursos = Curso.objects.filter(profesor__pk = self.request.user.pk)
        kwargs['cursos']= cursos
        kwargs['usuario'] = self.request.user
        kwargs['rol'] = 'profesor'
        return kwargs

    def get_success_url(self):
        return reverse('admin_cursos:ver_cursos_profesor')

class EditCursoAyudante(UpdateView):
    model = Curso
    template_name = 'admin/cursos/create_curso_profesor.html'
    form_class = CrateCursoForm

    def get_context_data(self, **kwargs):
        kwargs = super(EditCursoAyudante, self).get_context_data(**kwargs)
        cursos = Curso.objects.filter(ayudante__pk = self.request.user.pk)
        kwargs['cursos']= cursos
        kwargs['usuario'] = self.request.user
        kwargs['rol'] = 'ayudante'
        return kwargs

    def get_success_url(self):
        return reverse('admin_cursos:ver_cursos_profesor')
    

class CursoListViewAlumno(TemplateView):
    template_name = 'admin/cursos/cursos_list_view.html'
    def get_context_data(self, **kwargs):
        kwargs = super(CursoListViewAlumno, self).get_context_data(**kwargs)
        cursos = Curso.objects.filter(estudiante__pk = self.request.user.pk)
        kwargs['cursos'] = cursos
        kwargs['usuario'] = self.request.user
        kwargs['rol'] = 'estudiante'
        return kwargs

class CursoListViewProfesor(TemplateView):
    template_name = 'admin/cursos/cursos_list_view_profesor.html'
    def get_context_data(self, **kwargs):
        kwargs = super(CursoListViewProfesor, self).get_context_data(**kwargs)
        cursos = Curso.objects.filter(profesor__pk = self.request.user.pk)
        kwargs['cursos'] = cursos
        kwargs['usuario'] = self.request.user
        kwargs['rol'] = 'profesor'
        return kwargs

class CursoListViewAyudante(TemplateView):
    template_name = 'admin/cursos/cursos_list_view_profesor.html'
    def get_context_data(self, **kwargs):
        kwargs = super(CursoListViewAyudante, self).get_context_data(**kwargs)
        cursos = Curso.objects.filter(ayudante__pk = self.request.user.pk)
        kwargs['cursos'] = cursos
        kwargs['usuario'] = self.request.user
        kwargs['rol'] = 'ayudante'
        return kwargs

class CambiarClaveUser(UpdateView):
    model = User
    template_name = 'admin/usuarios/cambiar_password.html'
    form_class = CambiarPasswordForm

    def get_context_data(self, **kwargs):
        kwargs = super(CambiarClaveUser, self).get_context_data(**kwargs)
        kwargs['usuario']=self.object
        return kwargs
    
    def get_form_kwargs(self):
        kwargs = super(CambiarClaveUser, self).get_form_kwargs()
        kwargs.update({'usuario':self.object})
        return kwargs
    
    def form_valid(self, form):
        estudiante = form.save(commit=False)
        password = estudiante.password
        estudiante.set_password(password)
        estudiante.save()
        messages.success(self.request, 'Contraseña cambiada.')
        return HttpResponseRedirect(self.get_success_url()) 

    def get_success_url(self):
        return reverse('admin_coevaluaciones:ver_coevaluaciones_alumno')
    

class AgregarGrupoCursoView(FormView):
    model = Curso
    form_class = CrateGroupForm
    pk_url_kwarg = 'curso_pk'
    template_name = 'admin/cursos/agregar_grupo_curso_profesor.html'

    def get_context_data(self, **kwargs):
        kwargs = super(AgregarGrupoCursoView, self).get_context_data(**kwargs)
        curso_pk = self.kwargs.get(self.pk_url_kwarg, None)
        try:
            filter_query  = Q(profesor__pk = self.request.user.pk ) | Q(ayudante__pk = self.request.user.pk )
            curso = Curso.objects.filter(filter_query).get(pk=curso_pk)
        except Curso.DoesNotExist:
            raise Http404

        kwargs['curso'] = curso
        kwargs['profesor'] = curso.profesor
        kwargs['ayudante'] = curso.ayudante
        kwargs['rol'] = 'ayudante'
        kwargs['usuario'] = self.request.user
        return kwargs

    def form_valid(self, form):
        curso_pk = self.kwargs.get(self.pk_url_kwarg, None)
        try:
            filter_query  = Q(profesor__pk = self.request.user.pk ) | Q(ayudante__pk = self.request.user.pk )
            curso = Curso.objects.filter(filter_query).get(pk=curso_pk)
        except Curso.DoesNotExist:
            raise Http404
        grupo = form.save(commit=False)
        grupo.curso = curso
        grupo.save()
        return redirect('admin_cursos:ver_curso_profesor', curso_pk=curso_pk)


class DesasignarEstudanteGrupoCursoViewProfesor(RedirectView):
    def get_redirect_url(self, *args, **kwargs):
        curso_pk = self.kwargs.pop('curso_pk',None)
        try:
            filter_query  = Q(profesor__pk = self.request.user.pk ) | Q(ayudante__pk = self.request.user.pk )
            curso = Curso.objects.filter(filter_query).get(pk=curso_pk)
        except Curso.DoesNotExist:
            raise Http404
        estudiante = get_object_or_404(Estudiante, pk=self.kwargs.pop('estudiante_pk',None))
        estudiante.grupo_set.remove(estudiante.grupo_set.get(curso__pk=curso.pk))
        estudiante.save()
        return reverse('admin_cursos:ver_curso_profesor', kwargs={'curso_pk':curso.pk})


class EditarGrupoCursoView(UpdateView):
    form_class = CrateGroupForm
    model = Grupo
    pk_url_kwarg = 'grupo_pk'
    pk_parent_url_kwarg = 'curso_pk'
    template_name = 'admin/cursos/editar_grupo_curso_profesor.html'

    def get_success_url(self):
        curso_pk = self.kwargs.get(self.pk_parent_url_kwarg, None)
        return reverse('admin_cursos:ver_curso_profesor', kwargs={self.pk_parent_url_kwarg: curso_pk})

    def get_context_data(self, **kwargs):
        kwargs = super(EditarGrupoCursoView, self).get_context_data(**kwargs)
        curso_pk = self.kwargs.get(self.pk_parent_url_kwarg, None)
        try:
            filter_query  = Q(profesor__pk = self.request.user.pk ) | Q(ayudante__pk = self.request.user.pk )
            curso = Curso.objects.filter(filter_query, grupo__pk=self.object.pk).get(pk=curso_pk)
        except Curso.DoesNotExist:
            raise Http404

        kwargs['curso'] = curso
        kwargs['profesor'] = curso.profesor
        kwargs['ayudante'] = curso.ayudante
        kwargs['usuario'] = self.request.user
        kwargs['rol'] = 'ayudante'
        return kwargs


class EliminarGrupoCursoViewProfesor(RedirectView):
    def get_redirect_url(self, *args, **kwargs):
        curso_pk = self.kwargs.pop('curso_pk',None)
        try:
            filter_query  = Q(profesor__pk = self.request.user.pk ) | Q(ayudante__pk = self.request.user.pk )
            curso = Curso.objects.filter(filter_query).get(pk=curso_pk)
        except Curso.DoesNotExist:
            raise Http404
        grupo = get_object_or_404(Grupo, pk=self.kwargs.pop('grupo_pk',None))
        estudiante = get_object_or_404(Estudiante, pk=self.kwargs.pop('estudiante_pk',None))
        estudiante.grupo_set.filter(curso__pk=curso.pk).clear()
        estudiante.save()
        return reverse('admin_cursos:ver_curso_profesor', kwargs={'curso_pk':curso.pk})


class AgregarEstudanteGrupoCursoViewProfesor(RedirectView):
    permenant=False

    def get_redirect_url(self, *args, **kwargs):
        curso_pk = self.kwargs.pop('curso_pk',None)
        try:
            filter_query  = Q(profesor__pk = self.request.user.pk ) | Q(ayudante__pk = self.request.user.pk )
            curso = Curso.objects.filter(filter_query).get(pk=curso_pk)
        except Curso.DoesNotExist:
            raise Http404
        grupo = get_object_or_404(Grupo, pk=self.kwargs.pop('grupo_pk',None))
        estudiante = get_object_or_404(Estudiante, pk=self.kwargs.pop('estudiante_pk',None))
        curso_pk=self.kwargs.pop('curso_pk',None)
        try:
            estudiante.grupo_set.remove(estudiante.grupo_set.get(curso__pk=curso.pk))
        except Grupo.DoesNotExist:
            pass # continue
        estudiante.grupo_set.add(grupo)
        estudiante.save()
        return reverse('admin_cursos:ver_curso_profesor', kwargs={'curso_pk':curso.pk})

class ImportarCursoView(FormView):
    template_name = 'admin/cursos/alumnos_import.html'
    form_class = XlsInputAlumnosForm

    def get_context_data(self, **kwargs):
        kwargs = super(ImportarCursoView, self).get_context_data(**kwargs)
        curso_pk = self.kwargs.get('curso_pk', None)
        curso = Curso.objects.filter(profesor__pk = self.request.user.pk).get(pk=curso_pk)
        kwargs['curso'] = curso
        kwargs['profesor'] = self.request.user
        kwargs['usuario'] = self.request.user
        kwargs['rol'] = 'profesor'
        return kwargs

    def form_valid(self, form):

        try:
            curso = Curso.objects.get(pk= self.kwargs.pop("curso_pk",None))
        except Curso.DoesNotExist:
            curso=None

        if curso:
            
            input_excel = form.cleaned_data['input_excel']
            cursos_excel = xlrd.open_workbook(file_contents=input_excel.read())
            curso_hoja = cursos_excel.sheet_by_index(0)

            #leo el documento
            #obtengo la primera Hoja
            # en la primera fila
            # for row in range(1, curso_hoja.nrows):
            row_index = 0

            for row in range(5, curso_hoja.nrows):
                try:
                    estudiante, created=Estudiante.objects.get_or_create(username=curso_hoja.cell_value(row,0))
                    if created:
                        estudiante.set_password(curso_hoja.cell_value(row,0)[-4:])
                        estudiante.is_staff = True 
                        groups = Group.objects.get(name="Alumno")
                        estudiante.groups.add(groups)

                    estudiante.first_name = curso_hoja.cell_value(row,1)
                    estudiante.last_name = curso_hoja.cell_value(row,2)
                    estudiante.email = curso_hoja.cell_value(row,3)
                    estudiante.curso.add(curso)
                    if curso_hoja.cell_value(row,4):
                        try:
                            estudiante.grupo_set.clear()
                            grupo = Grupo.objects.get(curso=curso, nombre__icontains=curso_hoja.cell_value(row,4))
                            estudiante.grupo_set.add(grupo)
                        except Exception:
                            pass
                    estudiante.save()
                except Exception:
                    pass
            messages.success(self.request, 'Alumnos importados exitosamente.')
            return redirect('admin_cursos:ver_curso_profesor', curso_pk=curso.pk)

class ExportarCursoView(View):
    def get(self, request, *args, **kwargs):
        book = xlwt.Workbook(encoding='utf8')
        sheet = book.add_sheet('Alumnos')  

        curso_pk = self.kwargs.get('curso_pk', None)
        curso = Curso.objects.filter(profesor__pk = self.request.user.pk).get(pk=curso_pk)
        # Adding style for cell
        # Create Alignment
        alignment = xlwt.Alignment()
        # horz May be: HORZ_GENERAL, HORZ_LEFT, HORZ_CENTER, HORZ_RIGHT,    
        # HORZ_FILLED, HORZ_JUSTIFIED, HORZ_CENTER_ACROSS_SEL,
        # HORZ_DISTRIBUTED
        alignment.horz = xlwt.Alignment.HORZ_LEFT
        # May be: VERT_TOP, VERT_CENTER, VERT_BOTTOM, VERT_JUSTIFIED,
        # VERT_DISTRIBUTED
        alignment.vert = xlwt.Alignment.VERT_TOP
        col_width = 256 * 21                        # 20 characters wide
        try:
            for i in range(0,4):
                sheet.col(i).width = col_width
        except ValueError:
            pass
        style = xlwt.XFStyle() # Create Style
        style.alignment = alignment # Add Alignment to Style
        # write the header
        header = ['Curso:', curso.nombre]
        for hcol, hcol_data in enumerate(header): # [(0,'Header 1'), (1, 'Header 2'), (2,'Header 3'), (3,'Header 4')]
           sheet.write(0, hcol, hcol_data, style=xlwt.Style.default_style)
        header = ['Profesor:', "%s %s"%(curso.profesor.first_name, curso.profesor.last_name)]
        for hcol, hcol_data in enumerate(header): # [(0,'Header 1'), (1, 'Header 2'), (2,'Header 3'), (3,'Header 4')]
           sheet.write(1, hcol, hcol_data, style=xlwt.Style.default_style)
        
        header = ['Semestre:', "%s"%(curso.semestre)]
        for hcol, hcol_data in enumerate(header): # [(0,'Header 1'), (1, 'Header 2'), (2,'Header 3'), (3,'Header 4')]
           sheet.write(2, hcol, hcol_data, style=xlwt.Style.default_style)


        header = ['Rut (nombre de usuario)', 'Nombre', 'Apellido', 'email', 'grupo opcional']
        for hcol, hcol_data in enumerate(header): # [(0,'Header 1'), (1, 'Header 2'), (2,'Header 3'), (3,'Header 4')]
           sheet.write(4, hcol, hcol_data, style=xlwt.Style.default_style)

        # write your data, you can also get it from your model
        data = [['1111111', 'prueba', 'usuario 1', 'prueba1@example.com'],
                ['2222222', 'prueba', 'usuario 2', 'prueba2@example.com'],
                ['333333k', 'prueba', 'usuario 3', 'prueba3@example.com'],
                ['4444444', 'prueba', 'usuario 4', 'prueba4@example.com'],
                ]
        for row, row_data in enumerate(data, start=5): # start from row no.1
           for col, col_data in enumerate(row_data):
                 sheet.write(row, col, col_data, style=xlwt.Style.default_style)


        response = HttpResponse(mimetype='application/vnd.ms-excel')
        response['Content-Disposition'] = 'attachment; filename=ejemplo-subida.xls'
        book.save(response)
        return response
