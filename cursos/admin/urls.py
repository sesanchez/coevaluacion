# -*- coding: utf-8 -*-
from django.conf.urls import url, patterns
from django.contrib import admin

from .views import CursoDetailViewProfesor, CursoListViewAlumno, CursoListViewProfesor, AgregarGrupoCursoView,\
AgregarEstudanteGrupoCursoViewProfesor, ImportarCursoView, ExportarCursoView, CrearCursoProfesorView, DesasignarEstudanteGrupoCursoViewProfesor, \
     EditarGrupoCursoView
from cursos.admin.views import CambiarClaveUser

urlpatterns = patterns('',#Estudiante
     url(r'^cursos/alumno/ver/$', admin.site.admin_view(CursoListViewAlumno.as_view()), name='ver_cursos_alumno'),
     url(r'^cursos/profesor/ver/$', admin.site.admin_view(CursoListViewProfesor.as_view()), name='ver_cursos_profesor'),
     url(r'^cursos/(?P<curso_pk>\d+)/profesor/ver/$', admin.site.admin_view(CursoDetailViewProfesor.as_view()), name='ver_curso_profesor'),
     url(r'^cursos/(?P<curso_pk>\d+)/profesor/agregar/grupo/$', admin.site.admin_view(AgregarGrupoCursoView.as_view()), name='agregar_grupo_a_curso'),
     url(r'^cursos/(?P<curso_pk>\d+)/profesor/editar/grupo/(?P<grupo_pk>\d+)/$', admin.site.admin_view(EditarGrupoCursoView.as_view()), name='editar_grupo_de_curso'),
     url(r'^cursos/(?P<curso_pk>\d+)/profesor/agregar/estudiante/(?P<estudiante_pk>\d+)/grupo/(?P<grupo_pk>\d+)$', admin.site.admin_view(AgregarEstudanteGrupoCursoViewProfesor.as_view()), name='asignar_alumno_grupo_curso'),
     url(r'^cursos/(?P<curso_pk>\d+)/profesor/desasignar/estudiante/(?P<estudiante_pk>\d+)/$', admin.site.admin_view(DesasignarEstudanteGrupoCursoViewProfesor.as_view()), name='desasignar_alumno_grupo_curso'),
     url(r'^usuario/clave/cambiar/(?P<pk>\d+)$', admin.site.admin_view(CambiarClaveUser.as_view()), name='cambiar_clave'),
     
     url(r'^cursos/(?P<curso_pk>\d+)/profesor/importar/alumnos/$', admin.site.admin_view(ImportarCursoView.as_view()), name='importar_alumnos'),
     url(r'^cursos/(?P<curso_pk>\d+)/profesor/descargar/alumnos/$', admin.site.admin_view(ExportarCursoView.as_view()), name='descargar_ejemplo_alumnos'),
     
     url(r'^cursos/profesor/crear-curso/$', admin.site.admin_view(CrearCursoProfesorView.as_view()), name='crear_curso_profesor'),

    )