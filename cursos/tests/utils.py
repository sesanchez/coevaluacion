from cursos.models import Profesor, Curso, Estudiante, Grupo
from django.contrib.auth.models import Group
from django.utils.crypto import random


class CursoMixin:
    alumnos = []
    grupos = []

    def create_profesor(self):
        self.profesor = Profesor.objects.create_user(username='testprofesor', password='testprofesor')
        self.profesor.is_active = True
        self.profesor.is_staff = True
        group = Group.objects.get(name='Profesor') 
        self.profesor.groups.add(group)
        self.profesor.save()
        return self.profesor
    
    def create_curso(self):
        try:
            profesor = self.profesor
            self.curso = Curso(profesor=profesor, nombre='testcurso', semestre='semestre_prueba')
            self.curso.save()
            return self.curso
        except:
            return None

    def asignar_alumno(self, alumno):
        try:
            alumno.curso = self.curso
            alumno.save()
            return alumno
        except:
            return None

    def create_alumno(self):
        try:
            curso = self.curso
            alumno= Estudiante.objects.create_user(username=u'testcurso_testalumno%d'%len(self.alumnos), password=u'testcurso_testalumno%d'%len(self.alumnos), email=u'testcurso_testalumno%s@example.com'%len(self.alumnos))
            alumno.is_active = True
            alumno.is_staff = True
            group = Group.objects.get(name='Estudiante') 
            alumno.groups.add(group)
            alumno.save()
            alumno.curso.add(curso)
            alumno.save()
            self.alumnos.append(alumno)
            return alumno
        except:
            return None
    
    def create_grupo(self):
        try:
            curso = self.curso
            grupo = Grupo(identificador=len(self.grupos), nombre="testcurso_testgrupo%d"%len(self.grupos))
            grupo.curso = curso
            grupo.save()
        except:
            return None

    def asignar_grupos(self):
        for alumno in self.alumnos:
            alumno.grupo_set.add(random.choice(self.grupos))
            alumno.save()

    def validar_curso(self):
        try:
            if self.curso.validar_curso():
                return True
            else:
                for grupo in self.grupos:
                    if grupo.alumnos.count() == 1:
                        alumno = Estudiante.objects.create_user(username=u'testcurso_testalumno%d'%len(self.alumnos), password=u'testcurso_testalumno%d'%len(self.alumnos), email=u'testcurso_testalumno%s@example.com'%len(self.alumnos))
                        alumno.is_active = True
                        alumno.is_staff = True
                        group = Group.objects.get(name='Estudiante') 
                        alumno.groups.add(group)
                        alumno.curso
                        alumno.save()
                        alumno.curso.add(self.curso)
                        alumno.grupo_set.add(grupo)
                        alumno.save()
                        self.alumnos.append(alumno)
                return self.curso.validar_curso()
        except:
            return False


    def clear(self):
        self.profesor.delete()
        self.curso.delete()
        delete_alumno = [alumno.delete() for alumno in self.alumnos]
        delete_grupo = [grupo.delete() for grupo in self.grupo]
        return True
    