from django.conf import settings
from django import template

register = template.Library()

### Filters

@register.filter
def obtener_grupo( estudiante, curso_pk ):
	return estudiante.get_grupo_curso(curso_pk)

@register.filter
def obtener_grupo_name( estudiante, curso_pk ):
	if estudiante.get_grupo_curso(curso_pk):
		return estudiante.get_grupo_curso(curso_pk)
	else:
		return"sin grupo"


@register.filter(name='addcss')
def addcss(field, css):
   return field.as_widget(attrs={"class":css})

@register.filter(is_safe=True)
def label_with_classes(value, arg):
    return value.label_tag(attrs={'class': arg})