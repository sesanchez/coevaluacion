# -*- coding: utf-8 -*-
import datetime
from south.db import db
from south.v2 import SchemaMigration
from django.db import models


class Migration(SchemaMigration):

    def forwards(self, orm):
        # Adding model 'Profesor'
        db.create_table(u'cursos_profesor', (
            (u'user_ptr', self.gf('django.db.models.fields.related.OneToOneField')(to=orm['auth.User'], unique=True, primary_key=True)),
        ))
        db.send_create_signal(u'cursos', ['Profesor'])

        # Adding model 'Semestre'
        db.create_table(u'cursos_semestre', (
            (u'id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('year', self.gf('django.db.models.fields.DateField')()),
            ('name', self.gf('django.db.models.fields.CharField')(max_length=100)),
        ))
        db.send_create_signal(u'cursos', ['Semestre'])

        # Adding model 'Curso'
        db.create_table(u'cursos_curso', (
            (u'id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('profesor', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['cursos.Profesor'])),
            ('nombre', self.gf('django.db.models.fields.CharField')(max_length=100)),
            ('semestre', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['cursos.Semestre'])),
        ))
        db.send_create_signal(u'cursos', ['Curso'])

        # Adding model 'Estudiante'
        db.create_table(u'cursos_estudiante', (
            (u'user_ptr', self.gf('django.db.models.fields.related.OneToOneField')(to=orm['auth.User'], unique=True, primary_key=True)),
        ))
        db.send_create_signal(u'cursos', ['Estudiante'])

        # Adding model 'Grupo'
        db.create_table(u'cursos_grupo', (
            (u'id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('curso', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['cursos.Curso'])),
            ('identificador', self.gf('django.db.models.fields.PositiveSmallIntegerField')()),
        ))
        db.send_create_signal(u'cursos', ['Grupo'])

        # Adding unique constraint on 'Grupo', fields ['curso', 'identificador']
        db.create_unique(u'cursos_grupo', ['curso_id', 'identificador'])

        # Adding M2M table for field alumnos on 'Grupo'
        m2m_table_name = db.shorten_name(u'cursos_grupo_alumnos')
        db.create_table(m2m_table_name, (
            ('id', models.AutoField(verbose_name='ID', primary_key=True, auto_created=True)),
            ('grupo', models.ForeignKey(orm[u'cursos.grupo'], null=False)),
            ('estudiante', models.ForeignKey(orm[u'cursos.estudiante'], null=False))
        ))
        db.create_unique(m2m_table_name, ['grupo_id', 'estudiante_id'])


    def backwards(self, orm):
        # Removing unique constraint on 'Grupo', fields ['curso', 'identificador']
        db.delete_unique(u'cursos_grupo', ['curso_id', 'identificador'])

        # Deleting model 'Profesor'
        db.delete_table(u'cursos_profesor')

        # Deleting model 'Semestre'
        db.delete_table(u'cursos_semestre')

        # Deleting model 'Curso'
        db.delete_table(u'cursos_curso')

        # Deleting model 'Estudiante'
        db.delete_table(u'cursos_estudiante')

        # Deleting model 'Grupo'
        db.delete_table(u'cursos_grupo')

        # Removing M2M table for field alumnos on 'Grupo'
        db.delete_table(db.shorten_name(u'cursos_grupo_alumnos'))


    models = {
        u'auth.group': {
            'Meta': {'object_name': 'Group'},
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'unique': 'True', 'max_length': '80'}),
            'permissions': ('django.db.models.fields.related.ManyToManyField', [], {'to': u"orm['auth.Permission']", 'symmetrical': 'False', 'blank': 'True'})
        },
        u'auth.permission': {
            'Meta': {'ordering': "(u'content_type__app_label', u'content_type__model', u'codename')", 'unique_together': "((u'content_type', u'codename'),)", 'object_name': 'Permission'},
            'codename': ('django.db.models.fields.CharField', [], {'max_length': '100'}),
            'content_type': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['contenttypes.ContentType']"}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '50'})
        },
        u'auth.user': {
            'Meta': {'object_name': 'User'},
            'date_joined': ('django.db.models.fields.DateTimeField', [], {'default': 'datetime.datetime.now'}),
            'email': ('django.db.models.fields.EmailField', [], {'max_length': '75', 'blank': 'True'}),
            'first_name': ('django.db.models.fields.CharField', [], {'max_length': '30', 'blank': 'True'}),
            'groups': ('django.db.models.fields.related.ManyToManyField', [], {'to': u"orm['auth.Group']", 'symmetrical': 'False', 'blank': 'True'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'is_active': ('django.db.models.fields.BooleanField', [], {'default': 'True'}),
            'is_staff': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'is_superuser': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'last_login': ('django.db.models.fields.DateTimeField', [], {'default': 'datetime.datetime.now'}),
            'last_name': ('django.db.models.fields.CharField', [], {'max_length': '30', 'blank': 'True'}),
            'password': ('django.db.models.fields.CharField', [], {'max_length': '128'}),
            'user_permissions': ('django.db.models.fields.related.ManyToManyField', [], {'to': u"orm['auth.Permission']", 'symmetrical': 'False', 'blank': 'True'}),
            'username': ('django.db.models.fields.CharField', [], {'unique': 'True', 'max_length': '30'})
        },
        u'contenttypes.contenttype': {
            'Meta': {'ordering': "('name',)", 'unique_together': "(('app_label', 'model'),)", 'object_name': 'ContentType', 'db_table': "'django_content_type'"},
            'app_label': ('django.db.models.fields.CharField', [], {'max_length': '100'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'model': ('django.db.models.fields.CharField', [], {'max_length': '100'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '100'})
        },
        u'cursos.curso': {
            'Meta': {'object_name': 'Curso'},
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'nombre': ('django.db.models.fields.CharField', [], {'max_length': '100'}),
            'profesor': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['cursos.Profesor']"}),
            'semestre': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['cursos.Semestre']"})
        },
        u'cursos.estudiante': {
            'Meta': {'object_name': 'Estudiante', '_ormbases': [u'auth.User']},
            u'user_ptr': ('django.db.models.fields.related.OneToOneField', [], {'to': u"orm['auth.User']", 'unique': 'True', 'primary_key': 'True'})
        },
        u'cursos.grupo': {
            'Meta': {'unique_together': "(('curso', 'identificador'),)", 'object_name': 'Grupo'},
            'alumnos': ('django.db.models.fields.related.ManyToManyField', [], {'to': u"orm['cursos.Estudiante']", 'symmetrical': 'False'}),
            'curso': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['cursos.Curso']"}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'identificador': ('django.db.models.fields.PositiveSmallIntegerField', [], {})
        },
        u'cursos.profesor': {
            'Meta': {'object_name': 'Profesor', '_ormbases': [u'auth.User']},
            u'user_ptr': ('django.db.models.fields.related.OneToOneField', [], {'to': u"orm['auth.User']", 'unique': 'True', 'primary_key': 'True'})
        },
        u'cursos.semestre': {
            'Meta': {'object_name': 'Semestre'},
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '100'}),
            'year': ('django.db.models.fields.DateField', [], {})
        }
    }

    complete_apps = ['cursos']