from django.views.generic import View
import xlrd
import xlwt
from django.http.response import HttpResponse

class BajarExcelView(View):
    file_name = 'descarga'
    
    def get_book(self):
        raise NotImplementedError('No implementado get book')

    def get(self, request, *args, **kwargs):
        book = self.get_book()
        response = HttpResponse(mimetype='application/vnd.ms-excel')
        response['Content-Disposition'] = u'attachment; filename=%s.xls'%self.file_name
        book.save(response)
        return response